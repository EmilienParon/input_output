#input_output is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import oemof.solph as oemof_solph
import numpy as np

class Flow:

    def __init__(self, name, market):
        """
            Flow objects match any real world flow in the system (money, matter, energy...).

            Args:
                name (str): the name of the flow.
                market (Market): the market which it comes from or is going to.
        """
        self.name = name
        self.market = market
        self.oemof_flow = oemof_solph.Flow()
        self.values = None

class FixedFlow(Flow):

    def __init__(self, name, market, volume):
        """
            FixedFlow are flows that have their values fixed to a certain value in the optimization.
            They can be used for example to model the demand in goods and services of households, supposed to be met at all times.

            Args:
                name (str): the name of the flow.
                market (Market): the market which it comes from or is going to.
                volume (float): the fixed volume of the flow.
        """
        super().__init__(name, market)
        # If a volume is provided, fix the nominal value in oemof
        # (the flow will be forced to this value in the optimization)
        self.oemof_flow.fix = np.array([volume])/np.array([volume]).max()
        self.oemof_flow.nominal_value = volume
        self.oemof_flow.fixed = True

class VariableFlow(Flow):

    def __init__(self, name, market, coefficient=None, max_volume=None, cost=None):
        """
            VariableFlow are flows that can be varied in the optimization, in order to match the necessary volume to balance all flows.
            They can be part of a process with inputs and outputs, and be defined relatively to an output ReferenceVariableFlow, by providing a coefficient value.
            They can be part of a process with only inputs or only outputs, and be defined independantly by providing a max_volume.

            Args:
                name (str): the name of the flow.
                market (Market): the market which it comes from or is going to.
                coefficient (float): the ratio between the ReferenceVariableFlow of the process.
                max_volume (float) : the maximum value that the flow can take in the optimization process.
                cost (float) : the coast of the flow. If None the cost is set to 0. The solver will optimize the system in order to get the minimal cost.
        """
        super().__init__(name, market)
        self.oemof_flow.fixed = False
        if coefficient is not None:
            self.coefficient = coefficient
        elif max_volume is not None:
            self.oemof_flow.nominal_value = max_volume
        if cost is not None:
            self.oemof_flow.variable_costs= [cost]
    
class ReferenceVariableFlow(Flow):

    def __init__(self, name, market, max_volume, fixed=None):
        """
            ReferenceVariableFlow are like VariableFlows, but are used as reference in a process :
            the values of all other flows are defined relatively to the value of this reference flow.

            Args:
                name (str): the name of the flow.
                market (Market): the market which it comes from or is going to.
                max_volume (float) : the maximum value that the flow can take in the optimization process.
                fixed (str) : determine if the flow is fixed or not
        """
        super().__init__(name, market)
        # If the flow is the reference flow for a process, set the nominal value in oemof
        # to allow the optimization to vary the flow value between 0 and max_volume.
        if fixed is not None:
            self.oemof_flow.fixed = True
            self.oemof_flow.fix = np.array([max_volume])/np.array([max_volume]).max()
        else:
            self.oemof_flow.fixed = False
            
        self.coefficient = 1.0
        self.oemof_flow.nominal_value = max_volume
        
