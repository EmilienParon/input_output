#input_output is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import oemof.solph as oemof_solph
import uuid
from .flow import ReferenceVariableFlow 
from .market import Market
from .flow import FixedFlow, VariableFlow

import numpy as np

class Process:

    def __init__(self, name, input_flows=[], output_flows=[], activity=None):
        """
            The Process object matches the real world transformation of input products into output products.

            Args:
                name (str): process name.
                input_flows (list): Flow objects describing the products consumption of the process.
                output_flows (list): 
                    Flow objects describing the products production of the process.
                    One of them must be registered as a reference_flow and have a max_volume :
                    all flows will be scaled according to this reference flow and the coefficient of each flow.
                activity (Activity): the activity in which the process is taking place.
        """
        self.name = name
        self.id = str(uuid.uuid1())
        self.activity = activity
        self.input_flows = input_flows
        self.output_flows = output_flows
        
        # Throw an error if more than one flow is a reference flow, or if no reference flow is found
        if len(input_flows) > 0 and len(output_flows) > 0:
            k = 0
            for flow in self.output_flows:
                if isinstance(flow, ReferenceVariableFlow) is True:
                    k += 1

            if k == 0:
                raise ValueError("Error in process " + self.name + " : no reference flow was found in its outputs.")
            elif k > 1:
                raise ValueError("Error in process " + self.name + " : multiple reference flows found in the outputs (there should be only one reference flow).")


    def setup(self, oemof_system):
        """
            Add the oemof components (Transformer, Source, Sink) to the oemof model, for each component. 

            Args:
                oemof_system: the oemof siumation object.
        """

        inputs = {}
        outputs = {}
        conversion_factors = {}

        # If the process has inputs and outputs, setup an oemof Transformer
        if len(self.input_flows) > 0 and len(self.output_flows) > 0:

            for input_flow in self.input_flows:
                inputs[input_flow.market.oemof_bus] = input_flow.oemof_flow
                conversion_factors[input_flow.market.oemof_bus] = input_flow.coefficient

            for output_flow in self.output_flows:
                outputs[output_flow.market.oemof_bus] = output_flow.oemof_flow
                conversion_factors[output_flow.market.oemof_bus] = output_flow.coefficient

            oemof_component = oemof_solph.Transformer(
                label=self.id,
                inputs=inputs,
                outputs=outputs,
                conversion_factors=conversion_factors
            )

        # If the process has only inputs, setup an oemof Sink
        elif len(self.input_flows) > 0:

            for input_flow in self.input_flows:
                inputs[input_flow.market.oemof_bus] = input_flow.oemof_flow

            oemof_component = oemof_solph.Sink(
                label=self.id,
                inputs=inputs
            )

        # If the process has only outputs, setup an oemof Source
        elif len(self.output_flows) > 0:

            for output_flow in self.output_flows:
                outputs[output_flow.market.oemof_bus] = output_flow.oemof_flow

            oemof_component = oemof_solph.Source(
                label=self.id,
                outputs=outputs
            )

        # Add the omeof component to the oemof simulation object
        oemof_system.add(oemof_component)

        # Store the name of the process
        self.system.nodes[self.id] = self.name


    def extract_results(self, results):
        """
            Extract the values of the flows of the process.

            Args:
                results (pandas.DataFrame): dataframe of results produced by the solve method of the System object.
        """


        res = results[["from","to","value"]].to_numpy()
        
        # print([x.market.id for x in self.input_flows])
        # market_ids = [x.market.id for x in self.input_flows]
        # input_flows = np.empty((len(market_ids), 3),dtype=str)
        # input_flows[:,1] = market_ids
            
        # input_flows[:,2] = res[np.logical_and(res[:, 0]== input_flows[:,1], res[:, 1]== self.id),2]
        for input_flow in self.input_flows:
            
            # mask = (results["from"] == input_flow.market.id) & (results["to"] == self.id)
            # input_flow.values = results.loc[mask, "value"]
            input_flow.values = res[np.logical_and(res[:, 0]== input_flow.market.id, res[:, 1]== self.id),2]


        for output_flow in self.output_flows:
            # mask = (results["from"] == self.id) & (results["to"] == output_flow.market.id)
            # output_flow.values = results.loc[mask, "value"]
            output_flow.values = res[np.logical_and(res[:, 0]== self.id, res[:, 1]== output_flow.market.id),2]


class StockProcess(Process):

    def __init__(self, name, input_flows, activity=None):
        super().__init__(name, input_flows)
        self.market_charge = Market(name + "-charge")
        self.market_discharge = Market(name + "-discharge")

    def setup(self, oemof_system):

        # Add the internal bus / market to oemof
        self.market_charge.system = self.system
        self.market_charge.setup(oemof_system)

        self.market_discharge.system = self.system
        self.market_discharge.setup(oemof_system)

        # Add a storage component
        oemof_component = oemof_solph.components.GenericStorage(
            label=self.id + "-storage",
            inputs={self.market_discharge.oemof_bus: oemof_solph.Flow()},
            outputs={self.market_discharge.oemof_bus: oemof_solph.Flow()},
            nominal_storage_capacity=1e12,
            initial_storage_level=None,
            balanced=False
        )

        self.system.nodes[self.id + "-storage"] = self.name + "-storage"

        oemof_system.add(oemof_component)

        # Add a transformer component between the external market (providing the product)
        # and the internal markets (where the product is stored or consumed)
        inputs = {}
        outputs = {}
        conversion_factors = {}

        # input_flow = self.input_flows[0]
        for input_flow in self.input_flows:
            inputs[input_flow.market.oemof_bus] = input_flow.oemof_flow
            conversion_factors[input_flow.market.oemof_bus] = input_flow.coefficient

        # output_flow = ReferenceVariableFlow(name="charge", market=self.market_charge.oemof_bus, max_volume=1e9)
        outputs[self.market_charge.oemof_bus] = oemof_solph.Flow()
        conversion_factors[self.market_charge.oemof_bus] = 1.0

        # output_flow = ReferenceVariableFlow(name="discharge", market=self.market_discharge.oemof_bus, max_volume=1e9)
        outputs[self.market_discharge.oemof_bus] = oemof_solph.Flow()
        conversion_factors[self.market_discharge.oemof_bus] = 1.0

        oemof_component = oemof_solph.Transformer(
            label=self.id + "-link",
            inputs=inputs,
            outputs=outputs,
            conversion_factors=conversion_factors
        )

        self.system.nodes[self.id + "-link"] = self.name + "-link"

        # Add the omeof component to the oemof simulation object
        oemof_system.add(oemof_component)

        # Store the name of the process
        self.system.nodes[self.id] = self.name


