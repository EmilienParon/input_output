#input_output is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import oemof.solph as oemof_solph
from oemof.solph.processing import create_dataframe
import pandas as pd
import plotly.graph_objects as go
import plotly.express as px
from pkg_resources import resource_filename
from pathlib import Path

class System:

    def __init__(self, time_index):
        """
            The System object is the global simulation object of the input_output package.
            It is used for balancing the supply and demand for products for all geographical regions, activities and markets, by solving an optimization model.
            It stores the oemof object, which handles the optimization model, and all regions.

            Args:
                time_index (pd.datetime): date time index of the simulation (ex : 10 years from 2020 to 2029).
        """
        # Store the time index and create a dummy hourly time index with the same length for oemof
        # (oemof only works for constant time increments (seconds, minutes, hours...))
        self.time_index = time_index
        oemof_time_index = pd.date_range('1/1/2020', periods=len(time_index), freq='H')

        # Initialize the oemof simulation object
        self.oemof_system = oemof_solph.EnergySystem(timeindex=oemof_time_index)

        # Prepare the regions storage dicts
        self.regions = {}

        # Prepare results dataframe storage
        self.results = None

        # Prepare a dict to store oemof unique ids <-> "nodes" names (buses, processes)
        self.nodes = {}

    def add_regions(self, regions):
        """
            Stores Region objects in a dict (key : region name, value : region object).

            Args:
                regions (list): a list of Region objects.
        """
        for region in regions:
            region.system = self
            self.regions[region.id] = region

    def setup(self):
        """
            Add the oemof components contained in each region's activities to the oemof model.
        """
        for k, region in self.regions.items():
            region.setup(oemof_system=self.oemof_system)

    def extract_results(self, results):
        """
            Extract the values of the flows of each process, activity and region.

            Args:
                results (pandas.DataFrame): dataframe of results produced by the solve method.
        """
        for k, region in self.regions.items():
            region.extract_results(results)
            
    def localize_markets(self):
        """
            Create a dict {market_label: region_label}
            
            Returns:
                dict: locatization of markets
        """
        markets = {}
        for region in self.regions.keys():
            markets = dict(markets, **self.regions[region].localize_markets())
        return markets
    
    def localize_processes(self):
        """
            Create a dict {process_labek: region_label}
            
            Returns:
                dict: locatization of processes
        """
        processes = {}
        for region in self.regions.keys():
            for activity in self.regions[region].activities:
                for process in self.regions[region].activities[activity].processes.keys():
                    processes = dict(processes, **{process: self.regions[region].name})
        return processes

    def solve(self):
        """
            Solve the optimization problem to balance all flows in the system.
        """

        # create optimization model
        optimization_model = oemof_solph.Model(energysystem = self.oemof_system)

        # solve problem
        optimization_model.solve(
            solver = 'cbc',
            solver_io = 'lp',
            solve_kwargs = {'tee': True, 'keepfiles': True}
        )

        term = str(self.oemof_system.results["Solver"][0]["Termination condition"])

        if term == "infeasible":
            raise ValueError("The problem is infeasible. Check if the activities have the capacity to produce at least as much as the demand, for each market of the system.")

        else:
            df = create_dataframe(optimization_model)
            df = df[df.variable_name == 'flow'].copy()
            df['from'] = df['pyomo_tuple'].apply(lambda x: x[2][0].label)
            df['to'] = df['pyomo_tuple'].apply(lambda x: x[2][1].label)
            
            results = df[['from', 'to', 'timestep', 'value']]

            # Add the node labels
            nodes_labels = pd.DataFrame({
                "id": list(self.nodes.keys()),
                "label": list(self.nodes.values())
            })

            results = results.set_index("from").join(nodes_labels.set_index("id")).reset_index()
            results.columns = ["from", "to", "timestep", "value", "from_label"]

            results = results.set_index("to").join(nodes_labels.set_index("id")).reset_index()
            results.columns = ["from", "to", "timestep", "value", "from_label", "to_label"]
            
            results[["from_region","to_region"]] = results[["from_label", "to_label"]]
            results[["from_region","to_region"]] = results[["from_region","to_region"]].replace({**self.localize_markets() , **self.localize_processes()})        

            results = results[["from", "from_label", "from_region", "to", "to_label", "to_region", "value"]]
            
            # Store the results dataframe
            self.results = results

            # Store the flow values in each Flow object
            self.extract_results(results)

from .activity import Region, Activity
from .market import Market
from .carbon import carbon_setup
from .flow import *
from .process import *
import os

class Neighborhood(System):
    
    def __init__(self, time_index):
        """
            The Neighborhood object is the global simulation object of the input_output package for a specific neighborhood within the actual input_output economical world.
            It is used for balancing the supply and demand for products for all geographical regions, activities and markets, by solving an optimization model.
            It stores the oemof object, which handles the optimization model, and all regions.

            Args:
                time_index (pd.datetime): date time index of the simulation (ex : 10 years from 2020 to 2029).
        """
        
        super().__init__(time_index)
        self.program = None
        self.housing_data = None
        self.mobility = None
        self.conso_hh = None
        
        self.flows = None
        self.carbon_factors= None
        self.N=None
        
        script_path = os.path.abspath(__file__)
        self.path = os.path.dirname(os.path.dirname(script_path))
        
        # Input datas (create a specific folder with specific project datas)
        # Eurostat coefficients : GHG_row = GHG_france and GHG(CPA_D)_row = GHG(CPA_D)_france
        # Those coefficients were fixed in order to get an average french carbon footprint of 10.2tCO2e/year (1 households_activity)
        # by considering that 1tCO2e/year due to building construction is missing
        coefficients = pd.read_excel(self.path+"/input_output/data/neighborhood/coef_17062020.xlsx")
        
        
        # -------------------
        
        # Add regions
        self.rest_of_the_world = Region(name="rest_of_the_world")
        self.france = Region(name="france")
        self.biosphere = Region(name= "biosphere")
        self.neighborhood = Region(name="neighborhood")
        self.add_regions([self.rest_of_the_world,self.france, self.biosphere, self.neighborhood])
        lab_markets = pd.DataFrame([["rest_of_the_world", self.rest_of_the_world], ["france", self.france], ['biosphere', self.biosphere], ['neighborhood', self.neighborhood]], columns = ['territory', 'market_location'])
        
        # -------------------
        
        # Eurostat coefficients  
        # Add the missing line of households activities ADD THE FOLLOWING LINES IN THE COEFFICIENTS CREATION SCRIPT
        a = pd.DataFrame([['households_activity', 1, 'activity', 'hh_activity', 'production', 'france', 'france']], columns = coefficients.columns.tolist())
        coefficients = coefficients.append(a)
        # Add construction emissions for households activities ADD THE FOLLOWING LINES IN THE COEFFICIENTS CREATION SCRIPT
        a = pd.DataFrame([['HH_BUILD', 675, 'biosphere', 'CO2', 'other', 'france', 'activity'],
                          ['HH_BUILD', 1, 'activity', 'HH_BUILD', 'other', 'france', 'activity'],
                          ['households_activity', 1, 'france', 'HH_BUILD', 'production', 'france', 'activity']], columns = coefficients.columns.tolist())
        coefficients = coefficients.append(a)
        self.coefficients = coefficients
        # Extract CPA_O France from P3_S13 (see part with add_consumers where we had the corresponding budget)
        self.CPA_O_in_P3_S13 = self.coefficients.loc[(self.coefficients['activity'] == 'P3_S13_production_activity') & (self.coefficients['product'] == 'CPA_O') & (self.coefficients["from"] == "france"), 'coefficient'].values[
            0]  #we store the original value to use it in add_consumers
        # remove the CPA_O France for P3_S13_production_activity now that it has been extract
        self.coefficients = self.coefficients.loc[~((self.coefficients['activity'] == 'P3_S13_production_activity') & (self.coefficients['product'] == 'CPA_O') & (self.coefficients["from"] == "france"))]

        self.ghg = self.coefficients.loc[self.coefficients['from']=='biosphere', "product"].unique().tolist()
        
        # -------------------

        # Create markets
        self.markets = pd.DataFrame()
        for i in self.coefficients['product'].unique():
            if i in self.ghg:
                self.create_market("market_"+i+"_biosphere", i, self.biosphere)
            else:    
                self.create_market("market_"+i+"_france", i, self.france)
                self.create_market("market_"+i+"_rest_of_the_world", i, self.rest_of_the_world)
                
        
        # -------------------
        
        # Add production activities
        production = self.coefficients.loc[self.coefficients['sub_activity'].isin(['production', 'other'])]

        
        
        for i in production['activity'].unique():
            
            data = production.loc[production['activity']==i]
            acti = data.loc[data['from']=='activity']
            
            for j in acti['territory']:
                atype = acti['sub_activity'].values[0]
                ter = self.coefficients.loc[(self.coefficients['territory']==j)&(self.coefficients['activity']==i)]
                # Output product and market of the activity
                product = ter.loc[(ter['from']=='activity')&(ter['sub_activity']==atype), 'product'].values[0]
                mark = self.get_market(product, j)
                
                # Rearrange the coefficients to avoid loops on markets PEUT ETRE FAIT GLOBALEMENT SUR LA TABLE COEFFICIENTS ? 
                if ter.loc[(ter["product"]==product)&(ter['from']==j)&(ter['sub_activity']==atype), "coefficient"].size>0:
                    coef = ter.loc[(ter["product"]==product)&(ter['from']==j)&(ter['sub_activity']==atype), "coefficient"].values[0]
                    ter['coef'] = ter.loc[(ter['from']=='activity')&(ter['sub_activity']==atype), 'coefficient'].values[0]-coef
                    ter.loc[(ter["from"]=='activity')&(ter['sub_activity']==atype), 'coef'] = 1       
                    ter = ter.drop(ter.loc[(ter["product"]==product)&(ter['from']==j)&(ter['sub_activity']==atype)].index.values.astype(int)[0])
                    ter['coefficient'] = ter['coefficient']/ter['coef']
                    ter = ter.drop(columns = 'coef')  
                
                eu_activity = Activity(
                    name= i+"_"+j,
                    region= lab_markets.loc[lab_markets['territory']==j, "market_location"].values[0]
                )
                
                # Capital formation, captial consumption and production
                fcf = ter.loc[ter['sub_activity']=='capital_formation']
                fcc = ter.loc[ter['sub_activity']=='capital_consumption']
                ter = ter.loc[ter['sub_activity']==atype]
                
                equi_markets = pd.DataFrame(columns = ['product', 'market_charge', 'market_discharge', 'coefficient'])
                for x in fcf['product'].unique():
                    fcf_bis = fcf.loc[fcf['product']==x].reset_index(drop=True)
                    process_equipment = StockProcess(
                        name = i+"_"+x+"_equipment",
                        input_flows=[(VariableFlow("fixed_capital_formation", market = self.get_market(x, fcf_bis['from'][k]), coefficient=fcf_bis['coefficient'].values[k]/fcf_bis["coefficient"].sum())) for k in fcf_bis.index.values.tolist()],
                    )
                    eu_activity.add_processes([process_equipment])
                    equi_markets = equi_markets.append(pd.DataFrame([[x, process_equipment.market_charge, process_equipment.market_discharge, fcf_bis['coefficient'].sum()]], columns = ['product', 'market_charge', 'market_discharge', 'coefficient']), ignore_index=True)
                
                
                ter = ter.loc[ter['to']=='activity']
                # Drop the problem row in household activity
                ter = ter.loc[ter['from']!='activity']
                
                eu_process = Process(
                    name= product+"_prod_"+j,
                    input_flows=[(VariableFlow(ter['product'][x], market= self.get_market(ter['product'][x], ter['from'][x]), coefficient=ter['coefficient'][x])) for x in ter.index.values.tolist()] 
                    +[(VariableFlow("fixed_capital_formation_"+equi_markets['product'][k], market = equi_markets['market_charge'][k], coefficient = equi_markets['coefficient'][k])) for k in equi_markets.index.values.tolist()]
                    +[(VariableFlow("fixed_capital_consumption_"+fcc['product'][k], market = equi_markets.loc[equi_markets['product']==fcc['product'][k], "market_discharge"].values[0], coefficient = fcc['coefficient'][k])) for k in fcc.index.values.tolist()],
                    output_flows=[ReferenceVariableFlow(product, market= mark, max_volume=1e20)]
                )
                eu_activity.add_processes([eu_process])
                lab_markets.loc[lab_markets['territory']==j, "market_location"].values[0].add_activities([eu_activity])
                

                                                                               
        # -------------------
        
        # Create a producer of GHG
        ghg_producer = Activity(
            name = 'ghg_producer',
            region= self.biosphere
        )
        # Add a process for each gas
        for i in self.ghg:
            ghg_production = Process(
                name=i+"_production",
                output_flows=[ReferenceVariableFlow(i, market= self.get_market(i, self.biosphere), max_volume=1e20)]
            )
            
            ghg_producer.add_processes([ghg_production])
        
        self.biosphere.add_activities([ghg_producer]) 
        
        # -------------------
        
        
        
    
    def get_market(self, product, territory):
        """
            Find the market corresponding to a specific product and a specific location

            Args:
                product (str): CPA code of the product
                territory (Region or str) : Market location
                
            Returns: 
                market
        """
  
        lab_markets = pd.DataFrame([["rest_of_the_world", self.rest_of_the_world], ["france", self.france], ['biosphere', self.biosphere], ['neighborhood', self.neighborhood]], columns = ['territory', 'market_location'])
        if isinstance(territory, str):
            territory = lab_markets.loc[lab_markets["territory"]==territory, "market_location"].values[0]
        
        market = self.markets.loc[(self.markets['product']==product)&(self.markets['territory']==territory), 'market']
        if len(market)==0:
            print("product :"+str(product))
            print("territory :" + str(territory))
            print('Error : No market was created')
        if len(market)>1:
            print('Error : At least 2 markets were created with same caracteristics')
        if len(market)==1:
            return market.values[0]
    
    
    def create_market(self, market_name, product, market_location):
        """
            Create a new market for a specific product and a specific location.
            Add the market and its characteristics to the markets list of the Neighborhood system

            Args:
                market_name (str): Name of the market
                product (str) : CPA code of the product
                market_location (Region): Market location
                
            Returns: 
                market : the new market created
        """
        # Create a new market with the given market name
        market = Market(market_name)
        # Add the market to the region correponding to the given market location
        market_location.add_markets([market])
        # Add the new market characteristics to the markets list of the system
        self.markets = self.markets.append(pd.DataFrame([[product, market_location, market]], columns = ['product', 'territory', 'market']))
        return market
        
    def add_buildings(self, housing_data = None, activities_data = None):
        """
            Create buildings within the neighborhood (housing buildings and others)
            with their specific carbon emissions factors (kgCO2e/m2)

            Args:
                housing_data (DataFrame): data about housing buildings and their carbon emissions
                
                                        columns: "building_id" (str): building identification label
                                                 "floor_area" (float): total floor area (m2)
                                                 "energy_emissions" (float): total carbon emissions due to energy consumptions (kgCO2e/year)
                                                 "construction_factor" (float) : construction factor of the building in kgCO2e/m2 (kgCO2e/m2)
                                                 "waste_water_emissions" (float) : total carbon emissions du to waste and water consumption, collect and treatment (kgCO2e/year)
                                                 
                activities_data (DataFrame): data about other buildings (shops, etc.) and their carbon emissions
                
                                        columns: "nace" (str): CPA code of the activity 
                                                 "surface" (float): total floor area (m2)
                                                 "energy_emissions" (float): total carbon emissions due to energy consumptions (kgCO2e/year)
                                                 "construction_factor" : construction factor of the building in kgCO2e/m2 (kgCO2e/m2)
                                                 "type" (str): production type ("pres" means production on local market / "prod" means production on global market = french market)
                                                 "max" (float): annual activity production (€)

        """
        # Compute the average construction emission factor (kgCO2e/m2)
        if housing_data is not None: 
            self.housing_data = housing_data.copy()
            self.construction_factor = ((self.housing_data['s_hab']*self.housing_data['construction_factor'])/self.housing_data['s_hab'].sum()).mean()
        else:
            self.construction_factor = 1100

        # Constructions emissions
        construction = self.create_market("market_construction_neighborhood", "construction", self.neighborhood)
        
        acti_constru = Activity(
            name="construction_prod_neighborhood",
            region=self.neighborhood
        )
        constru_prod = Process(
            name="construction_production_neighborhood",
            input_flows=[VariableFlow("CO2", market = self.get_market("CO2", self.biosphere), coefficient=self.construction_factor)], # Constant coefficient of 1100 kgCO2e/m2 built
            output_flows=[ReferenceVariableFlow("construction", market = construction, max_volume = 1e12)]
        )
        acti_constru.add_processes([constru_prod])
        
        building = self.create_market("market_building_neighborhood", "building", self.neighborhood)
       
        acti_building = Activity(
            name="building_prod_neighborhood",
            region=self.neighborhood
        )
        building_stock = StockProcess(
            name = "building_stock",
            input_flows=[VariableFlow("fixed_capital_formation", market = construction, coefficient=1)]
        )
        building_prod = Process(
            name="building_production_neighborhood",
            input_flows=[(VariableFlow("fixed_capital_formation_construction", market = building_stock.market_charge, coefficient=1))]
                        +[(VariableFlow("fixed_capital_consumption_construction", market = building_stock.market_discharge, coefficient=1/50))],
            output_flows=[ReferenceVariableFlow("building", market = building, max_volume = 1e12)]
        )
        acti_building.add_processes([building_stock, building_prod])
        
        self.neighborhood.add_activities([acti_constru, acti_building])
        
        # -----------------------------
        
        if housing_data is not None:
            # Neighborhood housing data
            housing_construction = self.create_market("market_housing_construction_neighborhood", "housing_construction", self.neighborhood)
            housing_energy_exploitation = self.create_market("market_housing_energy_exploitation_neighborhood", "housing_energy_exploitation", self.neighborhood)
            housing_waste_water_exploitation = self.create_market("market_housing_waste_water_exploitation_neighborhood", "housing_waste_water_exploitation", self.neighborhood)
            housing_data['energy_coef'] = housing_data['energy_emissions']/housing_data['s_hab']   # carbon factor due to energy consumption in kgCO2e/m2
            housing_data['waste_water_coef'] = housing_data['waste_water_emissions'] / housing_data['s_hab']  # carbon factor due to waste and water collect, consumption, treatment in kgCO2e/m2
            for i in housing_data['building_id'].unique():
                #construction contributor
                activity = Activity(
                    name= i+"_housing_construction",
                    region= self.neighborhood
                )
                data = housing_data.loc[housing_data['building_id']==i]
                process = Process(
                    name =i+"_housing_construction_neighborhood",
                    input_flows = [VariableFlow("CO2", market = self.get_market("CO2", self.biosphere), coefficient = data['construction_factor'].values[0]/50)], #50 years ACV
                    output_flows = [ReferenceVariableFlow("housing_construction_neighborhood", market=housing_construction, max_volume=data["s_hab"].values[0], fixed=1.0)]
                    #output_flows = [ReferenceVariableFlow("housing_neighborhood", market=housing, max_volume=data["s_hab"].values[0], fixed=1.0)]
                )
                activity.add_processes([process])
                self.neighborhood.add_activities([activity])

                #energy exploitation contributor
                activity = Activity(
                    name=i + "_housing_energy_exploitation",
                    region=self.neighborhood
                )
                data = housing_data.loc[housing_data['building_id'] == i]
                process = Process(
                    name=i + "_housing_energy_exploitation_neighborhood",
                    input_flows=[VariableFlow("CO2", market=self.get_market("CO2", self.biosphere), coefficient=data['energy_coef'].values[0])],
                    #output_flows=[ReferenceVariableFlow("housing_neighborhood", market=housing, max_volume=data["s_hab"].values[0], fixed=1.0)]
                    output_flows=[ReferenceVariableFlow("housing_energy_exploitation_neighborhood", market=housing_energy_exploitation, max_volume=data["s_hab"].values[0], fixed=1.0)]
                )
                activity.add_processes([process])
                self.neighborhood.add_activities([activity])

                # waste and water exploitation contributor
                activity = Activity(
                    name=i + "_housing_waste_water_exploitation",
                    region=self.neighborhood
                )
                data = housing_data.loc[housing_data['building_id'] == i]
                process = Process(
                    name=i + "_housing_waste_water_exploitation_neighborhood",
                    input_flows=[VariableFlow("CO2", market=self.get_market("CO2", self.biosphere), coefficient=data['waste_water_coef'].values[0])],
                    #output_flows=[ReferenceVariableFlow("housing_neighborhood", market=housing, max_volume=data["s_hab"].values[0], fixed=1.0)]
                    output_flows=[ReferenceVariableFlow("housing_waste_water_exploitation_neighborhood", market=housing_waste_water_exploitation, max_volume=data["s_hab"].values[0], fixed=1.0)]
                )
                activity.add_processes([process])
                self.neighborhood.add_activities([activity])
                
        # -----------------------------
        
        if activities_data is not None:
            # Activities program
            activities_data['energy_coef'] = activities_data['energy_emissions']/activities_data['max']  # carbon factor due to energy consumption in kgCO2e/€
            if "construction_factor" not in activities_data.columns.tolist():
                activities_data["construction_factor"] =1200
            activities_data["construction"] = activities_data["construction_factor"]*activities_data["surface"]/activities_data["max"] # carbon factor due to energy consumption in kgCO2e/€ 
            self.program = activities_data
            for i in self.program['nace'].unique():
                market = self.create_market("market_"+i+"_neighborhood", i, self.neighborhood)
                
                # Create an import/export activity from France to the market created in the neighborhood
                importt = Activity(
                    name = i+"_import",
                    region = self.france
                )
                imp = Process(
                    name = i+"_import",
                    input_flows = [VariableFlow(i, market = self.get_market(i, self.france), coefficient =1, cost=1.0)],
                    output_flows = [ReferenceVariableFlow(i, market=market, max_volume=1e20)]
                )
                importt.add_processes([imp])
                self.france.add_activities([importt])
                
                export = Activity(
                    name = i+"_export",
                    region = self.neighborhood
                )
                exp = Process(
                    name = i+"_export",
                    input_flows = [VariableFlow(i, market = market , coefficient =1, cost=10.0)],
                    output_flows = [ReferenceVariableFlow(i, market=self.get_market(i, self.france), max_volume=1e20)]
                )
                export.add_processes([exp])
                self.neighborhood.add_activities([export])
            
            # Add neighborhood's activities
            for i in self.program['nace']:
                product = i
                market = self.get_market(i, self.neighborhood)
                # Estimate carbon emissions of the activity using French data
                ter = self.coefficients.loc[(self.coefficients['activity']==i+"_production_activity")&(self.coefficients['territory']=="france")].reset_index(drop=True)
                
                # Rearrange the coefficients to avoid loops on markets
                if ter.loc[(ter["product"]==product)&(ter['from']=="france")&(ter['sub_activity']=="production"), "coefficient"].size>0:
                    coef = ter.loc[(ter["product"]==product)&(ter['from']=="france")&(ter['sub_activity']=="production"), "coefficient"].values[0]
                    ter['coef'] = ter.loc[(ter['from']=='activity')&(ter['sub_activity']=="production"), 'coefficient'].values[0]-coef
                    ter.loc[(ter["from"]=='activity')&(ter['sub_activity']=="production"), 'coef'] = 1       
                    ter = ter.drop(ter.loc[(ter["product"]==product)&(ter['from']=="france")&(ter['sub_activity']=="production")].index.values.astype(int)[0])
                    ter['coefficient'] = ter['coefficient']/ter['coef']
                    ter = ter.drop(columns = 'coef')  
                
                # Prioritize local consumption for the local activities
                ter.loc[(ter['product'].isin(self.program['nace'].tolist()))&(ter['from']=='france'), 'from'] = 'neighborhood'
                
                # Precise data about energy consumption and direct emissions
                ter = ter.loc[ter['product']!='CPA_D']
                ter = ter.loc[ter['product']!='CO2']
                ter = ter.loc[ter['product'] != 'CO2_BIO']
                ter = ter.loc[ter['product'] != 'HFC_CO2E']
                ter = ter.loc[ter['product'] != 'N2O_CO2E']
                ter = ter.loc[ter['product'] != 'CH4_CO2E']
                ter = ter.loc[ter['product'] != 'PFC_CO2E']
                ter = ter.loc[ter['product'] != 'NF3_SF6_CO2E']

                # Precise data about construction
                ter = ter.loc[ter['product']!='CPA_L']
                ter = ter.loc[ter['product'] != 'CPA_F'] #Add EP
                data = self.program.loc[self.program['nace']==i]
                # data['value'] = data['value']/self.program.loc[self.program['nace']==i, 'max'].values[0]
                
                # Create activity
                acti = Activity(
                    name = i+"_production_acti_neigh",
                    region = self.neighborhood
                )
                
                # Choose the output market between France and neighborhood depending on production type (depending on activity naf code)
                if self.program.loc[self.program['nace']==i, 'type'].values[0]=='prod':
                    market = self.get_market(i, self.france)
                
                #Capital formation, captial consumption and production
                fcf = ter.loc[ter['sub_activity']=='capital_formation']
                fcc = ter.loc[ter['sub_activity']=='capital_consumption']
                ter = ter.loc[ter['sub_activity']=='production']
            
                equi_markets = pd.DataFrame(columns = ['product', 'market_charge', 'market_discharge', 'coefficient'])
                for x in fcf['product'].unique():
                    fcf_bis = fcf.loc[fcf['product']==x].reset_index(drop=True)
                    process_equipment = StockProcess(
                        name = i+"_"+x+"_equipment",
                        input_flows=[(VariableFlow("fixed_capital_formation", market = self.get_market(x,fcf_bis['from'][k]), coefficient=fcf_bis['coefficient'].values[k]/fcf_bis["coefficient"].sum())) for k in fcf_bis.index.values.tolist()],
                    )
                    acti.add_processes([process_equipment])
                    equi_markets = equi_markets.append(pd.DataFrame([[x, process_equipment.market_charge, process_equipment.market_discharge, fcf_bis['coefficient'].sum()]], columns = ['product', 'market_charge', 'market_discharge', 'coefficient']), ignore_index=True)
                
                
                ter = ter.loc[ter['to']=='activity']
                # Define input flows depending on information data
                i_flows = [(VariableFlow(ter['product'][x], market= self.get_market(ter['product'][x], ter['from'][x]), coefficient=ter['coefficient'][x])) for x in ter.index.values.tolist()]
                i_flows = i_flows +[(VariableFlow("fixed_capital_formation_"+equi_markets['product'][k], market = equi_markets['market_charge'][k],coefficient = equi_markets['coefficient'][k])) for k in equi_markets.index.values.tolist()]
                i_flows = i_flows +[(VariableFlow("fixed_capital_consumption_"+fcc['product'][k], market = equi_markets.loc[equi_markets['product']==fcc['product'][k], "market_discharge"].values[0], coefficient = fcc['coefficient'][k])) for k in fcc.index.values.tolist()]
                i_flows = i_flows +[VariableFlow("CO2", market = self.get_market("CO2", self.biosphere), coefficient = data['energy_coef'].values[0])]
                i_flows = i_flows +[VariableFlow("CO2", market = self.get_market("CO2", self.biosphere), coefficient = data['construction'].values[0])] #Construction factor
                # Add a process
                prod = Process(
                    name = i+"_production_neighborhood",
                     input_flows=i_flows,
                    output_flows = [ReferenceVariableFlow(product, market = market, max_volume = self.program.loc[self.program['nace']==i, 'max'].values[0], fixed=1.0)]
                    
                )
    
                acti.add_processes([prod])
                self.neighborhood.add_activities([acti])
    
    def add_energies(self, energy):
        """
            Create specific activities and markets for electricity, heating and cooling within the neighborhood
            with the specific carbon emissions factors of the local energy systems

            Args:
                energy (DataFrame): data about local energies and their carbon emission factors
                                    columns: "energy" (str): energy label
                                             "factor" (float): GHG factor (kgCO2e/kWh)

        """
        #energy = pd.read_excel('./bruneseau.xlsx', sheet_name='energy')

        for i in energy['energy']:
            market = self.create_market("market_"+i+"_neighborhood",i, self.neighborhood)
            
            activity = Activity(
                name= i+"_prod_neighborhood",
                region= self.neighborhood
            )
            process = Process(
                name =i+"_production_neighborhood",
                input_flows = [VariableFlow("CO2", market = self.get_market("CO2", self.biosphere), coefficient=energy.loc[energy['energy']==i, "factor"].values[0])],
                output_flows = [ReferenceVariableFlow(i, market=market, max_volume=1e20)]
            )
            activity.add_processes([process])
            self.neighborhood.add_activities([activity])
    
    def add_activities(self, program, acti_ener=None):
        """
            Create specific activities and markets for the local activities within the neighborhood
            Link the local markets to the associated French markets to enable import and export
            
            Optional : Add their specific energy consumptions from local energy to replace CPA_D eurostat data
                ATTENTION : This is possible only if local energies were defined thanks to add_energies() first

            Args:
                program (DataFrame): data about local activities
                                    columns: "nace" (str): CPA code of the activity
                                             "type" (str): "pres" or "prod" ("pres" means production on local market / "prod" means production on global market = french market)
                                             "max_volume" (float): annual production (€)
                acti_ener (DataFrame): data about energy consumptions per activity
                                        columns: "nace" (str): CPA code of the activity
                                                 "energy" (str): energy label
                                                 "value" (float): annual consumption (kWh)

        """
        #acti_ener = pd.read_csv(path+'activities_Bruneseau.csv')

        # Activities program
        self.program = program
        for i in self.program['nace'].unique():
            market = self.create_market("market_"+i+"_neighborhood", i, self.neighborhood)
            
            # Create an import/export activity from France to the market created in the neighborhood
            importt = Activity(
                name = i+"_import",
                region = self.france
            )
            imp = Process(
                name = i+"_import",
                input_flows = [VariableFlow(i, market = self.get_market(i, self.france), coefficient =1, cost=1.0)],
                output_flows = [ReferenceVariableFlow(i, market=market, max_volume=1e20)]
            )
            importt.add_processes([imp])
            self.france.add_activities([importt])
            
            export = Activity(
                name = i+"_export",
                region = self.neighborhood
            )
            exp = Process(
                name = i+"_export",
                input_flows = [VariableFlow(i, market = market , coefficient =1, cost=10.0)],
                output_flows = [ReferenceVariableFlow(i, market=self.get_market(i, self.france), max_volume=1e20)]
            )
            export.add_processes([exp])
            self.neighborhood.add_activities([export])
        
        # Add neighborhood's activities
        for i in self.program['nace']:
            product = i
            market = self.get_market(i, self.neighborhood)
            # Estimate carbon emissions of the activity using French data
            ter = self.coefficients.loc[(self.coefficients['activity']==i+"_production_activity")&(self.coefficients['territory']=="france")].reset_index(drop=True)
            
            # Rearrange the coefficients to avoid loops on markets
            if ter.loc[(ter["product"]==product)&(ter['from']=="france")&(ter['sub_activity']=="production"), "coefficient"].size>0:
                coef = ter.loc[(ter["product"]==product)&(ter['from']=="france")&(ter['sub_activity']=="production"), "coefficient"].values[0]
                ter['coef'] = ter.loc[(ter['from']=='activity')&(ter['sub_activity']=="production"), 'coefficient'].values[0]-coef
                ter.loc[(ter["from"]=='activity')&(ter['sub_activity']=="production"), 'coef'] = 1       
                ter = ter.drop(ter.loc[(ter["product"]==product)&(ter['from']=="france")&(ter['sub_activity']=="production")].index.values.astype(int)[0])
                ter['coefficient'] = ter['coefficient']/ter['coef']
                ter = ter.drop(columns = 'coef')  
            
            # Prioritize local consumption for the local activities
            ter.loc[(ter['product'].isin(self.program['nace'].tolist()))&(ter['from']=='france'), 'from'] = 'neighborhood'
            
            # Precise data about energy consumption
            if acti_ener is not None:
                ter = ter.loc[ter['product']!='CPA_D']
                data = acti_ener.loc[acti_ener['nace']==i]
                data['value'] = data['value']/self.program.loc[self.program['nace']==i, 'max'].values[0]
            
            # Create activity
            acti = Activity(
                name = i+"_production_acti_neigh",
                region = self.neighborhood
            )
            
            # Choose the output market between France and neighborhood depending on production type (depending on activity naf code)
            if self.program.loc[self.program['nace']==i, 'type'].values[0]=='prod':
                market = self.get_market(i, self.france)
            
            #Capital formation, captial consumption and production
            fcf = ter.loc[ter['sub_activity']=='capital_formation']
            fcc = ter.loc[ter['sub_activity']=='capital_consumption']
            ter = ter.loc[ter['sub_activity']=='production']
        
            equi_markets = pd.DataFrame(columns = ['product', 'market_charge', 'market_discharge', 'coefficient'])
            for x in fcf['product'].unique():
                fcf_bis = fcf.loc[fcf['product']==x].reset_index(drop=True)
                process_equipment = StockProcess(
                    name = i+"_"+x+"_equipment",
                    input_flows=[(VariableFlow("fixed_capital_formation", market = self.get_market(x,fcf_bis['from'][k]), coefficient=fcf_bis['coefficient'].values[k]/fcf_bis["coefficient"].sum())) for k in fcf_bis.index.values.tolist()],
                )
                acti.add_processes([process_equipment])
                equi_markets = equi_markets.append(pd.DataFrame([[x, process_equipment.market_charge, process_equipment.market_discharge, fcf_bis['coefficient'].sum()]], columns = ['product', 'market_charge', 'market_discharge', 'coefficient']), ignore_index=True)
            
            
            ter = ter.loc[ter['to']=='activity']
            # Define input flows depending on information data
            i_flows = [(VariableFlow(ter['product'][x], market= self.get_market(ter['product'][x], ter['from'][x]), coefficient=ter['coefficient'][x])) for x in ter.index.values.tolist()]+[(VariableFlow("fixed_capital_formation_"+equi_markets['product'][k], market = equi_markets['market_charge'][k],coefficient = equi_markets['coefficient'][k])) for k in equi_markets.index.values.tolist()]+[(VariableFlow("fixed_capital_consumption_"+fcc['product'][k], market = equi_markets.loc[equi_markets['product']==fcc['product'][k], "market_discharge"].values[0], coefficient = fcc['coefficient'][k])) for k in fcc.index.values.tolist()]
            
            if acti_ener is not None:
                i_flows = i_flows +[(VariableFlow(data['energy'][x], market = self.get_market(data['energy'][x], self.neighborhood), coefficient = data['value'][x]))for x in data.index.values.tolist()]
            
            # Add a process
            prod = Process(
                name = i+"_production_neighborhood",
                 input_flows=i_flows,
                output_flows = [ReferenceVariableFlow(product, market = market, max_volume = self.program.loc[self.program['nace']==i, 'max'].values[0], fixed=1.0)]
                
            )

            acti.add_processes([prod])
            self.neighborhood.add_activities([acti])

    def add_consumers(self, hh_data=None, mobility=None):
        """
            Add french consumers corresponding to the global french population
            
            Optional : Add specific information about local consumers
            ATTENTION : For now defining housing first is mandatory + no link with mobility 

            Args:
                hh_data (DataFrame): data about local households
                                    columns: "household_id" (int): household identification number
                                             "building_id" (str): building identification label #Not necessary here ?
                                             "flat_area" (float): livable floor area of the household flat #Not necessary here ?
                                             "n_persons" (int): household size
                                             "code" (str): CPA code
                                             "budget_item" (float): total household budget per BdF category (€)
                mobility (DataFrame) : data about inhabitants mobility
                
            ATTENTION : peut-être prendre hh_data directement en données BdF et faire le changement ici
                        pour pouvoir mettre à 0 les codes BdF dans le cas de la mobilité des énergies etc. OUI

        """
    
        if mobility is not None:
            self.mobility = mobility
            # Remove consumption data which will be treated by LCA
            hh_data.loc[hh_data['budget_item_id'].isin(['07221', '073ZZ', '0741Z']), 'budget_item'] = 0
            hh_data.loc[hh_data['budget_item_id'].isin(['0711Z', '07121', '07211', '07221', '07231', '0724Z']), 'budget_item'] = 0

                
        if self.housing_data is not None:
            # Remove consumption data which will be treated by LCA
            hh_data.loc[hh_data['budget_item_id'].isin(['04111', '04121', '0431Z', '04441', '04500', '04511', '0452Z', '0453Z', '045AZ']), 'budget_item']=0
        # -----------------------------
        if hh_data is not None:
   
            # Convert budget from BdF to CPA
            liste = hh_data.columns.tolist()
            liste = [e for e in liste if e not in ('share', 'budget_item_id', 'budget_item')]
            hh_data_save = hh_data[liste].drop_duplicates()
            liste = liste+['code']
            
            # Load conversion of data from BdF (INSEE) to CPA 
            conversion = pd.read_excel(self.path+'/input_output/data/neighborhood/budget_to_NAF.xlsx')
            conversion = pd.melt(conversion, id_vars = ['code', 'libelle_budget'])
            conversion = pd.merge(conversion, conversion.groupby(['code'], as_index=False)['value'].sum().rename(columns={'value':'tot'}), on ='code', how='left')
            conversion['coef'] = conversion['value']/conversion['tot']
            conversion = conversion[['code', 'libelle_budget', 'variable', 'coef']].rename(columns={'code':'budget_item_id','variable':'code'})
            conversion = conversion.sort_values(by='budget_item_id')
            hh_data['budget_item_id'] = hh_data['budget_item_id'].astype(str)
            hh_data = pd.merge(hh_data, conversion, on= 'budget_item_id')


            # ATTENTION: multiplication by n_uc here!!!
            hh_data['budget_CPA'] = hh_data['budget_item']*hh_data['coef']*hh_data['n_uc']
            hh_data['code'] = 'CPA_' + hh_data['code']
            # fix code CPA for P3_S13 and P3_S15
            hh_data.loc[hh_data['code'] == "CPA_P3_S15", "code"] = "P3_S15"
            hh_data.loc[hh_data['code'] == "CPA_P3_S13", "code"] = "P3_S13"

            hh_data = hh_data.groupby(liste, as_index=False)['budget_CPA'].sum()

            # Population size
            npers = hh_data[['household_id', 'n_persons']].drop_duplicates()['n_persons'].sum()
            # Create a product consumer in the neighborhood
            consumer = Activity(
                name="consumer",
                region=self.neighborhood
            )
            
            # -----------------------------
    
            # Keep eurostat covered products
            hh_data = hh_data.loc[hh_data['code'].isin(self.coefficients['product'].unique())]

            # Add the same value for French administration to everyone
            hh_data.loc[hh_data['code'] == 'CPA_O', 'budget_CPA'] = self.coefficients.loc[(self.coefficients['activity'] == 'households_activity') & (self.coefficients['product'] == 'CPA_O'), 'coefficient'].values[0]
            hh_data = hh_data.loc[hh_data['code']!='CPA_L']

            # Extract CPA_O from S3_P13
            hh_P3_S13 = hh_data.loc[hh_data['code'] == 'P3_S13'][['household_id', 'budget_CPA']].rename(columns={'budget_CPA':'budget_CPA_P3_S13'})
            hh_data = pd.merge(hh_data, hh_P3_S13, on="household_id", how='left')
            hh_data.loc[hh_data['code'] == 'CPA_O', 'budget_CPA'] += self.CPA_O_in_P3_S13 * hh_data.loc[hh_data['code'] == 'CPA_O', 'budget_CPA_P3_S13']
            hh_data = hh_data.drop(columns=["budget_CPA_P3_S13"])

            # -----------------------------
            
            # ATTENTION : ONLY IF HOUSING WAS CREATED
            # Replace CPA_D by real building consumptions thanks to "housing" activities 
            if self.housing_data is not None:
                hh_data = hh_data.loc[hh_data['code']!='CPA_D'] #TODO also CPA_F (construction) ?
            # Save consumptions data for later carbon calculation
            if 'building_id' in hh_data.columns:
                self.conso_hh = hh_data[['household_id', 'building_id', 'n_persons', 'flat_area', 'code', 'budget_CPA']].copy()
            else:
                self.conso_hh = hh_data[['household_id', 'n_persons', 'code', 'budget_CPA']].copy()
            hh_data = hh_data.groupby(['code'], as_index=False)['budget_CPA'].sum()
            hh_data = hh_data.loc[hh_data['budget_CPA']>0]
            
            # -----------------------------
            
            # Households consommation ratio between France and row
            hhacti = self.coefficients.loc[self.coefficients['activity']=='households_activity']
            hhacti = pd.merge(hhacti, hhacti.groupby(['product'], as_index=False)['coefficient'].sum().rename(columns={'coefficient':'total'}), on='product', how='left')
            hhacti['part'] = hhacti['coefficient']/hhacti['total']
            
            # -----------------------------
            
            # Create an activity to take into account taxes and margins
            market_final = pd.DataFrame()
            for i in [word for word in self.coefficients['product'].unique().tolist() if (word.startswith('CPA') or word=="P3_S13" or word=="P3_S15")]:
                market = self.create_market("market_final_"+i, "final_"+i, self.france)
      
            # Separate taxes from the productive price
            tax = pd.read_excel(self.path+'/input_output/data/neighborhood/200602 - Table coeffs taxes et marges.xlsx', sheet_name='matrix')
            tax = pd.melt(tax, id_vars=tax.columns.tolist()[0])
            tax.columns = ['from', 'to', 'value']
            tax = tax.loc[tax['value']>0]
            tax = tax.replace("CPA_L68A",'CPA_L')
            tax = tax.loc[tax['from'].isin([word for word in self.coefficients['product'].unique().tolist() if (word.startswith('CPA') or word=="P3_S13" or word=="P3_S15")])]
            
            tax = pd.merge(tax, hhacti[['product', 'from', 'part']].rename(columns={'from':'territory', 'product':'to'}), on='to', how='left')
            tax.loc[tax['territory'].isna(), 'territory']='france'
            tax.loc[tax['part'].isna(), 'part']= 1
            tax['value'] = tax['value']*tax['part']
            
            if self.program is not None:
                # Put the priority on the neighborhood's activities when it comes to consumption on the French markets
                tax.loc[(tax['to'].isin(self.program['nace'].tolist()))&(tax['territory']=='france'), 'territory'] = 'neighborhood'
            
            final = Activity(
                name="final",
                region = self.france
            )
            for i in tax['from'].unique():
                taxi = tax.loc[tax['from']==i]
                process = Process(
                    name =i+"_final_production",
                    input_flows = [(VariableFlow(taxi['to'][x], market = self.get_market(taxi['to'][x], taxi['territory'][x]), coefficient = taxi['value'][x]))for x in taxi.index.values.tolist()],             
                    output_flows = [ReferenceVariableFlow(i+"_final", market= self.get_market("final_"+i, self.france), max_volume = 1e12)]
                )
                final.add_processes([process])
            self.france.add_activities([final])
            
            # -----------------------------
            # Add specific housing consumption if known otherwise add the average emissions (HH_HEAT, HH_OTH, HH_BUILD)
            i_flows = [FixedFlow(hh_data['code'][x], market=self.get_market("final_" + hh_data['code'][x], self.france), volume=hh_data['budget_CPA'][x]) for x in hh_data.index.values.tolist()] \

            if self.housing_data is not None:
                surface = self.housing_data[['building_id', 's_hab']].drop_duplicates()['s_hab'].sum()
                i_flows = i_flows\
                          + [FixedFlow("housing_construction", market=self.get_market("housing_construction", self.neighborhood), volume=surface)]\
                          + [FixedFlow("housing_energy_exploitation", market=self.get_market("housing_energy_exploitation", self.neighborhood), volume=surface)]\
                          + [FixedFlow("housing_waste_water_exploitation", market=self.get_market("housing_waste_water_exploitation", self.neighborhood), volume=surface)]
            else:
                i_flows = i_flows + [FixedFlow("HH_HEAT", market=self.get_market("HH_HEAT", self.france), volume=1.0 * npers)] + [FixedFlow("HH_BUILD", market=self.get_market("HH_BUILD", self.france), volume=1.0 * npers)] + [
                    FixedFlow("HH_OTH", market=self.get_market("HH_OTH", self.france), volume=1.0 * npers)] + [FixedFlow("HH_WASTE_WATER", market=self.get_market("HH_WASTE_WATER", self.france), volume=1.0 * npers)]
            # Add average mobility emission (HH_TRA) if data about mobility is not given
            if self.mobility is None:
                i_flows = i_flows + [FixedFlow("HH_TRA", market=self.get_market("HH_TRA", self.france), volume=1.0 * npers)]

            consumer_consumption = Process(
                name="consumers",
                input_flows= i_flows              
            )
            
            consumer.add_processes([consumer_consumption])
            self.neighborhood.add_activities([consumer])
        
        # -----------------------------
        
        if self.program is not None:
            # Create product consumers in France to absorb excess production from local activities
            french = Activity(
                name="french_consumers",
                region=self.france
            )
            i_flows = [VariableFlow(x, market=self.get_market(x, self.france), max_volume =1e12, cost=10) for x in self.program['nace'].tolist()]
            if hh_data is not None and self.housing_data is not None:
                i_flows = i_flows\
                          +[VariableFlow('housing_construction', market=self.get_market("housing_construction",self.neighborhood), max_volume =1e12, cost=10)]\
                          +[VariableFlow('housing_energy_exploitation', market=self.get_market("housing_energy_exploitation", self.neighborhood), max_volume=1e12, cost=10)]\
                          +[VariableFlow('housing_waste_water_exploitation', market=self.get_market("housing_waste_water_exploitation", self.neighborhood), max_volume=1e12, cost=10)]
            french_consumption = Process(
                name = "french_demand",
                input_flows= i_flows
                )
#
            french.add_processes([french_consumption])
            self.france.add_activities([french])
        
        # -----------------------------
        
        # Create average french households
        n_fr = 67064000 # French population
        if hh_data is not None:
            n_fr = n_fr-npers # Remove people from the neighborhood to avoid double count

        hh_fr = Activity(
            name="hh_france",
            region=self.france
        )
        
        hh_fr_consumption = Process(
            name = "hh_france_demand",
            input_flows=[FixedFlow('hh_activity', market=self.get_market('hh_activity', self.france), volume = n_fr)]
            )
        
        hh_fr.add_processes([hh_fr_consumption])
        self.france.add_activities([hh_fr])
        
        # -----------------------------
        
        # Create average french bdf person

        bdf = pd.read_excel(self.path + '/input_output/data/insee/tm_201/irsocbdf11_TM107.xls', skiprows=2, usecols=[0,1]).dropna()
        bdf.columns = ['budget_item_id', 'budget_item']
        # Filter out grouped items
        bdf["budget_item_id"] = bdf["budget_item_id"].str.split(" - ", 0).str.get(0)
        bdf = bdf[bdf["budget_item_id"].str.len() == 5]

        # ADD PUBLIC SERVICES (P3_S13 and P3_S15, CPA_O) "BUDGET" BASED ON THE INPUT OUTPUT COEFFICIENTS VALUES (same for all household)
        coefficients_path = resource_filename('urbanprint', 'usager/input_output/data/neighborhood/coef_17062020.xlsx')
        coefficients = pd.read_excel(coefficients_path)
        bdf_P3_S13 = pd.DataFrame({'budget_item_id': ["013ZZ"], 'budget_item': [coefficients.loc[(coefficients['product'] == 'P3_S13') & (coefficients['activity'] == 'households_activity'), 'coefficient'].values[0]]})
        bdf_P3_S15 = pd.DataFrame({'budget_item_id': ["015ZZ"], 'budget_item': [coefficients.loc[(coefficients['product'] == 'P3_S15') & (coefficients['activity'] == 'households_activity'), 'coefficient'].values[0]]})
        # P3_S13 and P3_S15 coefficient are given by person and not by household
        # 27.8 households for 66.4 people in 2010 it gives 2.4 pers/household
        bdf_P3_S13['budget_item'] = bdf_P3_S13['budget_item']/ (27.8 / 66.4)
        bdf_P3_S15['budget_item'] = bdf_P3_S15['budget_item'] / (27.8 / 66.4)

        bdf = pd.concat([bdf, bdf_P3_S13])
        bdf = pd.concat([bdf, bdf_P3_S15])

        #bdf['household_id'] = 'average_bdf'
        #bdf['n_persons'] = bdf['n_uc'] = 1
        # 27.8 households for 66.4 people in 2010 it gives 2.4 pers/household
        bdf['budget_item'] = bdf['budget_item']*27.8/66.4

        ecart_path = resource_filename('urbanprint', 'usager/households/budget/data/input/elioth/ecart.csv')
        ecart = pd.read_csv(ecart_path)
        ecart= pd.melt(ecart, id_vars = ['budget_item_id', 'ratio'], var_name='decile_FR')
        bdf["decile_FR"]="D5"
        bdf = pd.merge(bdf, ecart, on=['budget_item_id', 'decile_FR'], how='left')
        bdf['budget_item']= bdf['budget_item']*bdf['ratio']/bdf['value']
        bdf = bdf.drop(columns=['ratio', 'value'])
        
        bdf['budget_item_id'] = bdf['budget_item_id'].astype(str)
        bdf = pd.merge(bdf, conversion, on= 'budget_item_id')
        bdf['budget_CPA'] = bdf['budget_item']*bdf['coef']
        bdf = bdf.groupby(['code'], as_index=False)['budget_CPA'].sum()
        bdf['code'] = 'CPA_'+bdf['code']
        bdf.loc[bdf['code'] == "CPA_P3_S15", "code"] = "P3_S15"
        bdf.loc[bdf['code'] == "CPA_P3_S13", "code"] = "P3_S13"

        ## correction_factor to match the real french eurostat consumption repartition based on budget method (bdf)
        correction_factor = pd.read_excel(self.path + '/input_output/data/neighborhood/bdf_vs_eurostat_budget.xlsx')
        bdf = pd.merge(bdf, correction_factor[["code", "ratio_bdf_to_french"]], on='code', how='left')
        bdf['budget_CPA'] = bdf['budget_CPA'] / bdf["ratio_bdf_to_french"]

        # Keep eurostat covered products
        bdf = bdf.loc[bdf['code'].isin(self.coefficients['product'].unique())]
        bdf = bdf.loc[bdf['budget_CPA']>0]
        #Add base CPA_O
        bdf_CPA_O = pd.DataFrame({'code': ["CPA_O"], 'budget_CPA': [self.coefficients.loc[(self.coefficients['activity']=='households_activity')&(self.coefficients['product']=='CPA_O'), 'coefficient'].values[0]]})
        bdf = pd.concat([bdf, bdf_CPA_O]).reset_index().drop(columns=['index'])
        bdf = bdf.loc[bdf['code']!='CPA_L']

        # Extract CPA_O from S3_P13
        bdf_P3_S13 = bdf.loc[bdf['code'] == 'P3_S13']['budget_CPA'].values[0]
        bdf.loc[bdf['code'] == 'CPA_O', 'budget_CPA'] += self.CPA_O_in_P3_S13 * bdf_P3_S13
        
        hh_bdf = Activity(
            name="hh_bdf",
            region=self.france
        )

        hh_bdf_consumption = Process(
            name = "hh_bdf_demand",
            input_flows=[FixedFlow(bdf['code'][x], market=self.get_market("final_"+bdf['code'][x], self.france), volume=bdf['budget_CPA'][x]) for x in bdf.index.values.tolist()]
                        #+[FixedFlow("P3_S13", market=self.get_market("P3_S13", self.france), volume=self.coefficients.loc[(self.coefficients['product']=='P3_S13')&(self.coefficients['activity']=='households_activity'), 'coefficient'].values[0])]
                        #+[FixedFlow("P3_S15", market=self.get_market("P3_S15", self.france), volume=self.coefficients.loc[(self.coefficients['product']=='P3_S15')&(self.coefficients['activity']=='households_activity'), 'coefficient'].values[0])]
                        +[FixedFlow("HH_TRA", market=self.get_market("HH_TRA", self.france), volume=1.0)]
                        +[FixedFlow("HH_HEAT", market=self.get_market("HH_HEAT", self.france), volume=1.0)]
                        +[FixedFlow("HH_OTH", market=self.get_market("HH_OTH", self.france), volume=1.0)]
                        +[FixedFlow("HH_BUILD", market=self.get_market("HH_BUILD", self.france), volume=1.0)]
                        + [FixedFlow("HH_WASTE_WATER", market=self.get_market("HH_WASTE_WATER", self.france), volume=1.0)]

            )

        hh_bdf.add_processes([hh_bdf_consumption])
        self.france.add_activities([hh_bdf])
        self.bdf = bdf.copy()
        
    def carbon(self, x):
        # Carbon calculation 
        
        labels = pd.read_excel(self.path+'/input_output/data/neighborhood/nomenclatures.xlsx', sheet_name='Eurostat')
        
        
        if self.flows is None or self.carbon_factors is None or self.N is None:
            # Carbon calculation set up
            self.flows, self.carbon_factors, self.N = carbon_setup(x)
        
        def group_categories(average): 
            average_tot = pd.DataFrame()
            for j in average['category_0'].unique():
                z = average.loc[average['category_0']==j]
                for i in z['category_1'].unique():
                    a = z.loc[z['category_1']==i]
                    if len(a['code'])<=1:
                       average_tot = pd.concat([average_tot, a.loc[a['footprint_pers']>0].drop(columns=['code'])]) 
                    else:   
                        a = a.sort_values(by='footprint_pers')
                        a['sum'] = a['footprint_pers'].cumsum() 
                        b = a.loc[(a['sum']<a['sum'].values[len(a['sum'])-1]*0.2)&(a['footprint_pers']>0)]
                        if len(b['code'])==0:
                            average_tot = pd.concat([average_tot, a.loc[a['footprint_pers']>0].drop(columns=['code', 'sum'])])
                        else:
                            a = a.loc[~a['code'].isin(b['code'])]
                            b=b.groupby(['category_0', 'category_1'], as_index = False)['footprint_pers'].sum()
                            b['label_fr'] = "Autres"
                            average_tot = pd.concat([average_tot, a.loc[a['footprint_pers']>0].drop(columns=['code', 'sum']), b])
            average_tot['footprint_pers'] = average_tot['footprint_pers'].round(decimals=-1)    
            return average_tot     
        
        
        
        if (self.conso_hh is None) and (self.mobility is None) and (self.housing_data is None):
            hh_activity = self.coefficients.loc[(self.coefficients['activity']=='households_activity')&(self.coefficients['from']!='activity')]
            hh_activity['label'] = 'market_' + hh_activity['product'] +'_'+hh_activity['from']
            full = pd.merge(hh_activity[['product', 'label', 'coefficient']], self.carbon_factors[['label', 'emission_factor']], on='label', how='left')
            full['footprint'] = full['emission_factor']*full['coefficient']
            categ = pd.read_excel(self.path +'/input_output/data/neighborhood/200616 - Postes d\'émissions.xlsx', usecols='A, C, D')
            full = pd.merge(full.rename(columns={'product':'code'}), categ, on ='code', how='left')
            self.full = full
            
        else:
                    
            # Create the final consumption DataFrame
            self.conso_hh['label'] = "market_final_"+self.conso_hh['code']
           
            # Add housing data if known otherwise add average housing emission data
            if self.housing_data is not None:
                self.conso_hh = self.conso_hh[['household_id', 'building_id', 'flat_area', 'n_persons', 'code', 'label', 'budget_CPA']].copy()
                for i in self.conso_hh['household_id'].unique():
                    n = self.conso_hh.loc[self.conso_hh['household_id']==i, "n_persons"].values[0]
                    a = self.conso_hh.loc[self.conso_hh['household_id']==i, "flat_area"].values[0]
                    b = self.conso_hh.loc[self.conso_hh['household_id']==i, "building_id"].values[0]
        
                    #self.conso_hh = pd.concat([self.conso_hh, pd.DataFrame([[i, b, a, n, "P3_S13", "market_P3_S13_france", self.coefficients.loc[(self.coefficients['product']=='P3_S13')&(self.coefficients['activity']=='households_activity'), 'coefficient'].values[0]*n],
                    #                                              [i, b, a, n, "P3_S15", "market_P3_S15_france", self.coefficients.loc[(self.coefficients['product']=='P3_S15')&(self.coefficients['activity']=='households_activity'), 'coefficient'].values[0]*n],
                    #                                              ], columns=['household_id', 'building_id', 'flat_area', 'n_persons', 'code', 'label', 'budget_CPA'])])
               #
                    self.conso_hh = pd.concat([self.conso_hh, pd.DataFrame([[i, b, a, n, "housing_energy_exploitation", b+"_housing_energy_exploitation_neighborhood", a]], columns=['household_id', 'building_id', 'flat_area', 'n_persons', 'code', 'label', 'budget_CPA'])])

                    self.conso_hh = pd.concat(
                        [self.conso_hh, pd.DataFrame([[i, b, a, n, "housing_construction", b + "_housing_construction_neighborhood", a]], columns=['household_id', 'building_id', 'flat_area', 'n_persons', 'code', 'label', 'budget_CPA'])])

                    self.conso_hh = pd.concat([self.conso_hh, pd.DataFrame([[i, b, a, n, "housing_waste_water_exploitation", b + "_housing_waste_water_exploitation_neighborhood", a]],
                                                                           columns=['household_id', 'building_id', 'flat_area', 'n_persons', 'code', 'label', 'budget_CPA'])])

                    # Add general mobility emission data if mobility is not known
                    if self.mobility is None:
                        self.conso_hh = pd.concat([self.conso_hh, pd.DataFrame([[i, b, a, n, "HH_TRA", "market_HH_TRA_france", n]], columns=['household_id', 'building_id', 'flat_area', 'n_persons', 'code', 'label', 'budget_CPA'])])
            else:
                self.conso_hh = self.conso_hh[['household_id', 'n_persons', 'code', 'label', 'budget_CPA']].copy()
                for i in self.conso_hh['household_id'].unique():
                    n = self.conso_hh.loc[self.conso_hh['household_id']==i, "n_persons"].values[0]
        
                    self.conso_hh = pd.concat([self.conso_hh, pd.DataFrame([
                    # [i, n, "P3_S13", "market_P3_S13_france", self.coefficients.loc[(self.coefficients['product']=='P3_S13')&(self.coefficients['activity']=='households_activity'), 'coefficient'].values[0]*n],
                    #                                              [i, n, "P3_S15", "market_P3_S15_france", self.coefficients.loc[(self.coefficients['product']=='P3_S15')&(self.coefficients['activity']=='households_activity'), 'coefficient'].values[0]*n],
                                                                  [i, n, "HH_OTH","market_HH_OTH_france", n],
                                                                  ], columns=['household_id', 'n_persons', 'code', 'label', 'budget_CPA'])])
                
                    self.conso_hh = pd.concat([self.conso_hh, pd.DataFrame([[i, n, "HH_HEAT", "market_HH_HEAT_france", n], [i, n, "HH_BUILD", "market_HH_BUILD_france", n]], columns=['household_id', 'n_persons', 'code', 'label', 'budget_CPA'])])

                    self.conso_hh = pd.concat([self.conso_hh, pd.DataFrame([[i, n, "HH_WASTE_WATER", "market_HH_WASTE_WATER_france", n]], columns=['household_id', 'n_persons', 'code', 'label', 'budget_CPA'])])
                    # Add general mobility emission data if mobility is not known
                    if self.mobility is None:
                        self.conso_hh = pd.concat([self.conso_hh, pd.DataFrame([[i, n, "HH_TRA", "market_HH_TRA_france", n]], columns=['household_id', 'n_persons', 'code', 'label', 'budget_CPA'])])

            # Carbon emissions data for all the households in the sample
            full = pd.merge(self.conso_hh, self.carbon_factors[['label', 'emission_factor']], on='label', how='left')

            full['footprint'] = full['emission_factor']*full['budget_CPA']
            full = full.sort_values(by='household_id')
            full['footprint_pers'] = full['footprint']/full['n_persons']

            bru = full[['household_id', 'code', 'footprint_pers']].copy()
            if self.mobility is not None:
                self.mobility.loc[self.mobility["type"]=="mobilité longue distance", 'code'] = "HH_TRA_LONG"
                self.mobility.loc[self.mobility["type"] == "mobilité régulière et locale", 'code'] = "HH_TRA_SHORT"
                self.mobility = self.mobility.groupby(["individual_id","household_id", "type", "code"], as_index=False)["co2"].sum()  # in kgCO2.an
                #merge
                bru = pd.merge(bru[['household_id', 'code', 'footprint_pers']], self.mobility[['household_id', 'individual_id']], on='household_id', how='right')
                bru = pd.concat([bru[['household_id', 'individual_id', 'code', 'footprint_pers']], self.mobility[['household_id', 'individual_id', 'code', 'co2']].rename(columns={'co2':'footprint_pers'})])
            else:
                # Create an individual id 
                indiv = self.conso_hh[['household_id', 'n_persons']].drop_duplicates()
                indiv = indiv['household_id'].repeat(indiv['n_persons']).reset_index(drop=True).reset_index().rename(columns={'index':'individual_id'})
                bru = pd.merge(bru[['household_id', 'code', 'footprint_pers']], indiv[['household_id', 'individual_id']], on='household_id', how='right')


            categ = pd.read_excel(self.path +'/input_output/data/neighborhood/200616 - Postes d\'émissions.xlsx', usecols='A, C, D')
            bru = pd.merge(bru, categ, on ='code', how='left')
            bru_save=bru.copy()
            bru_tot = bru.groupby(['individual_id'], as_index=False)['footprint_pers'].sum().rename(columns={'footprint_pers':'tot'}).sort_values(by='tot', ascending=False).reset_index(drop=True).reset_index()
            bru = pd.merge(bru, bru_tot, on='individual_id', how='left')
            bru = bru.sort_values(by='index')
            bru = bru.loc[~bru['footprint_pers'].isna()]
            bru = bru.groupby(['household_id', 'individual_id', 'index', 'category_0'], as_index=False)['footprint_pers'].sum()
            
            
            fig = go.Figure()
            fig.add_trace(go.Scatter(
                x=bru.loc[bru['category_0']=='Bâtiment', 'index'], y=bru.loc[bru['category_0']=='Bâtiment', 'footprint_pers'],
                hoverinfo='x+y',
                mode='lines',
                line=dict(width=0.5, color='rgb(136,14,79)'),
                stackgroup='one', # define stack group
                name="Bâtiment"
            ))
            fig.add_trace(go.Scatter(
                x=bru.loc[bru['category_0']=='Transport', 'index'], y=bru.loc[bru['category_0']=='Transport', 'footprint_pers'],
                hoverinfo='x+y',
                mode='lines',
                line=dict(width=0.5, color='rgb(255,87,34)'),
                stackgroup='one', # define stack group
                name="Transport"
            ))
            
            fig.add_trace(go.Scatter(
                x=bru.loc[bru['category_0']=='Alimentation', 'index'], y=bru.loc[bru['category_0']=='Alimentation', 'footprint_pers'],
                hoverinfo='x+y',
                mode='lines',
                line=dict(width=0.5, color='rgb(255,193,7)'),
                stackgroup='one', # define stack group
                name="Alimentation"
            ))
            fig.add_trace(go.Scatter(
                x=bru.loc[bru['category_0']=='Biens de consommation', 'index'], y=bru.loc[bru['category_0']=='Biens de consommation', 'footprint_pers'],
                hoverinfo='x+y',
                mode='lines',
                line=dict(width=0.5, color='rgb(205,220,57)'),
                stackgroup='one', # define stack group
                name="Biens de consommation"
            ))
            fig.add_trace(go.Scatter(
                x=bru.loc[bru['category_0']=='Services', 'index'], y=bru.loc[bru['category_0']=='Services', 'footprint_pers'],
                hoverinfo='x+y',
                mode='lines',
                line=dict(width=0.5, color='rgb(76,175,80)'),
                stackgroup='one', # define stack group
                name="Services"
            ))
            fig.update_layout(yaxis_range=(0, 30000))
            fig.update_layout(title = "Empreinte carbone des habitants du quartier", xaxis_title ="Numéro de la personne", yaxis_title="Empreinte carbone [kgCO2e/pers.an]")
            #fig.show(renderer='png')   
            self.fig = fig
            self.carbon_footprints = bru_save
            self.bru = bru
            
            # Create a treemap of the average person with the different categories
            average = self.carbon_footprints.fillna(0).groupby(['code','category_0', 'category_1'], as_index=False)['footprint_pers'].mean()
            average = pd.merge(average, labels, on='code', how='left')
            
            if self.housing_data is None:
                # Household post traitement to have the same nomenclature decomposition of the footprint for average french and neighborhood user
                housing_energy_exploitation_df = pd.merge(categ[categ["code"] == 'housing_energy_exploitation'], labels[labels["code"] == 'housing_energy_exploitation'], on='code', how='left')
                housing_construction_df = pd.merge(categ[categ["code"] == 'housing_construction'], labels[labels["code"] == 'housing_construction'], on='code', how='left')
                housing_energy_exploitation_df["footprint_pers"] = average[average["code"] == "HH_HEAT"]["footprint_pers"].values[0] + average[average["code"] == "CPA_D"]["footprint_pers"].values[0] + \
                                                                   average[average["code"] == "HH_OTH"]["footprint_pers"].values[0]
                average.loc[average["code"] == "HH_HEAT", "footprint_pers"] = 0
                average.loc[average["code"] == "CPA_D", "footprint_pers"] = 0
                average.loc[average["code"] == "HH_OTH", "footprint_pers"] = 0

                housing_construction_df["footprint_pers"] = average[average["code"] == "HH_BUILD"]["footprint_pers"].values[0] + average[average["code"] == "CPA_F"]["footprint_pers"].values[0]
                average.loc[average["code"] == "HH_BUILD", "footprint_pers"] = 0
                average.loc[average["code"] == "CPA_F", "footprint_pers"] = 0

                average = pd.concat([average, housing_construction_df, housing_energy_exploitation_df]).reset_index().drop(columns=["index"])
                
            self.average_hab = average.copy()
            #self.average_hab = group_categories(average)
            average['base'] = 'Empreinte totale'
            self.tmap = px.treemap(average, path=['base', 'category_0', 'category_1', 'code'], values = 'footprint_pers', maxdepth=3,
                                   color = 'category_0', color_discrete_map={'(?)': 'white','Alimentation':'rgb(255,193,7)', 'Transport':'rgb(255,87,34)', 'Services':'rgb(76,175,80)', 'Biens de consommation':'rgb(205,220,57)', 'Bâtiment':'rgb(136,14,79)'})
            
            
        # Average french (1 households activity)
        hh_activity = self.coefficients.loc[(self.coefficients['activity']=='households_activity')&(self.coefficients['from']!='activity')]
        hh_activity['label'] = 'market_' + hh_activity['product'] +'_'+hh_activity['from']
        self.budget_french = hh_activity.copy().rename(columns={'product':'code'})
        average_french = pd.merge(hh_activity[['product', 'label', 'coefficient']], self.carbon_factors[['label', 'emission_factor']], on='label', how='left')
        average_french['footprint'] = average_french['emission_factor']*average_french['coefficient']
        average_french = pd.merge(average_french.rename(columns={'product':'code'}), categ, on ='code', how='left')
        average_french = average_french.groupby(['code', 'category_0', 'category_1'], as_index=False)['footprint'].sum().rename(columns={'footprint':'footprint_pers'})
        average_french = pd.merge(average_french, labels, on='code', how='left')
        # Household post traitement to have the same nomenclature decomposition of the footprint for average french and neighborhood user
        housing_energy_exploitation_df = pd.merge(categ[categ["code"] == 'housing_energy_exploitation'], labels[labels["code"] == 'housing_energy_exploitation'], on='code', how='left')
        housing_construction_df = pd.merge(categ[categ["code"] == 'housing_construction'], labels[labels["code"] == 'housing_construction'], on='code', how='left')
        housing_energy_exploitation_df["footprint_pers"] = average_french[average_french["code"] == "HH_HEAT"]["footprint_pers"].values[0] + average_french[average_french["code"] == "CPA_D"]["footprint_pers"].values[0] + \
                                                           average_french[average_french["code"] == "HH_OTH"]["footprint_pers"].values[0]
        average_french.loc[average_french["code"] == "HH_HEAT", "footprint_pers"] = 0
        average_french.loc[average_french["code"] == "CPA_D", "footprint_pers"] = 0
        average_french.loc[average_french["code"] == "HH_OTH", "footprint_pers"] = 0

        housing_construction_df["footprint_pers"] = average_french[average_french["code"] == "HH_BUILD"]["footprint_pers"].values[0] + average_french[average_french["code"] == "CPA_F"]["footprint_pers"].values[0]
        average_french.loc[average_french["code"] == "HH_BUILD", "footprint_pers"] = 0
        average_french.loc[average_french["code"] == "CPA_F", "footprint_pers"] = 0

        average_french = pd.concat([average_french, housing_construction_df, housing_energy_exploitation_df]).reset_index().drop(columns=["index"])

        self.average_french  = average_french.copy()
        #self.average_french = group_categories(average_french)
        
        average_bdf = self.bdf.copy()
        average_bdf['label'] = 'market_final_' +average_bdf['code']
        average_bdf = pd.concat([average_bdf,
                                 pd.DataFrame([
                                     #["P3_S13", self.coefficients.loc[(self.coefficients['product']=='P3_S13')&(self.coefficients['activity']=='households_activity'), 'coefficient'].values[0], "market_P3_S13_france"],
                                    #["P3_S15", self.coefficients.loc[(self.coefficients['product']=='P3_S15')&(self.coefficients['activity']=='households_activity'), 'coefficient'].values[0], "market_P3_S15_france"],
                                               ["HH_OTH", 1.0, "market_HH_OTH_france"],
                                               ["HH_TRA", 1.0, "market_HH_TRA_france"],
                                               ["HH_HEAT", 1.0, "market_HH_HEAT_france"],
                                               ["HH_BUILD", 1.0, "market_HH_BUILD_france"],
                                               ["HH_WASTE_WATER", 1.0, "market_HH_WASTE_WATER_france"]],
                                              columns=['code', 'budget_CPA', 'label']
                                              )
                                 ])
        average_bdf = pd.merge(average_bdf, self.carbon_factors[['label', 'emission_factor']], on='label', how='left')
        average_bdf['footprint_pers'] = average_bdf['emission_factor']*average_bdf['budget_CPA']
        average_bdf = pd.merge(average_bdf, categ, on ='code', how='left')
        average_bdf = average_bdf.groupby(['code', 'category_0', 'category_1'], as_index=False)['footprint_pers'].sum()
        average_bdf = pd.merge(average_bdf, labels, on='code', how='left')
        # Household post traitement to have the same nomenclature decomposition of the footprint for average bdf and neighborhood user
        housing_energy_exploitation_df = pd.merge(categ[categ["code"] == 'housing_energy_exploitation'], labels[labels["code"] == 'housing_energy_exploitation'], on='code', how='left')
        housing_construction_df = pd.merge(categ[categ["code"] == 'housing_construction'], labels[labels["code"] == 'housing_construction'], on='code', how='left')
        housing_energy_exploitation_df["footprint_pers"] = average_bdf[average_bdf["code"] == "HH_HEAT"]["footprint_pers"].values[0] + average_bdf[average_bdf["code"] == "CPA_D"]["footprint_pers"].values[0] + \
                                                           average_bdf[average_bdf["code"] == "HH_OTH"]["footprint_pers"].values[0]
        average_bdf.loc[average_bdf["code"] == "HH_HEAT", "footprint_pers"] = 0
        average_bdf.loc[average_bdf["code"] == "CPA_D", "footprint_pers"] = 0
        average_bdf.loc[average_bdf["code"] == "HH_OTH", "footprint_pers"] = 0

        housing_construction_df["footprint_pers"] = average_bdf[average_bdf["code"] == "HH_BUILD"]["footprint_pers"].values[0] + average_bdf[average_bdf["code"] == "CPA_F"]["footprint_pers"].values[0]
        average_bdf.loc[average_bdf["code"] == "HH_BUILD", "footprint_pers"] = 0
        average_bdf.loc[average_bdf["code"] == "CPA_F", "footprint_pers"] = 0

        average_bdf = pd.concat([average_bdf, housing_construction_df, housing_energy_exploitation_df]).reset_index().drop(columns=["index"])

        self.average_bdf  = average_bdf.copy()


        #POST_TREATMENT : CLEAN DATAFRAME TO AVOID DOUBLE COMPTABILITY

        #SEPARATION OF THE COLLECTION, CONSUMPTION AND TREATMENT OF WATER AND WASTE FROM P3_S13 GLOBAL PUBLIC SERVICES AND CPA_O (since P3_S13 have been ventilated to CPA_O
        waste_water_coef_persons = 149 #kgCO2e/per #Source : https://www.associationbilancarbone.fr/wp-content/uploads/2018/01/bonnes-pratiques-36p-v4ter-003.pdf D'apres l'inventaire nationale le traitement des déchets et eaux c'est 10 MTCO2e, soit 1.3% de l'empreinte carbone moyenne
        original_water_input_output_persons = 37 #Input output methode provide a mean of 37kgCO2e/hab just for water use. We had unplugged it from budget conversion to use the overall waste and water activity
        P3_S13 = self.coefficients[self.coefficients["activity"] == "P3_S13_production_activity"][["coefficient", "product"]]
        for product in P3_S13["product"]:
            # Estimate carbon emissions of the activity using French data
            P3_S13.loc[P3_S13["product"] == product, "E37_E39_coeff"] = P3_S13.loc[P3_S13["product"] == product, "coefficient"] * self.coefficients.loc[
                (self.coefficients['product'] == 'CPA_E37-39') & (self.coefficients['activity'] == str(product) + '_production_activity'), 'coefficient'].sum()
        P3_S13["E37_E39_ratio"] = P3_S13["E37_E39_coeff"] / P3_S13["E37_E39_coeff"].sum()

        CPA_O_ratio = P3_S13.loc[P3_S13["product"]=="CPA_O", "E37_E39_ratio"].values[0]

        self.average_bdf.loc[self.average_bdf["code"] == "CPA_O", "footprint_pers"] -= (CPA_O_ratio) * (waste_water_coef_persons - original_water_input_output_persons)
        self.average_french.loc[self.average_french["code"] == "CPA_O", "footprint_pers"] -= (CPA_O_ratio) * (waste_water_coef_persons - original_water_input_output_persons)
        self.average_hab.loc[self.average_hab["code"] == "CPA_O", "footprint_pers"] -= (CPA_O_ratio) * (waste_water_coef_persons - original_water_input_output_persons)

        self.average_bdf.loc[self.average_bdf["code"] == "P3_S13", "footprint_pers"] -= (1-CPA_O_ratio)*(waste_water_coef_persons - original_water_input_output_persons)
        self.average_french.loc[self.average_french["code"] == "P3_S13", "footprint_pers"] -= (1-CPA_O_ratio)*(waste_water_coef_persons - original_water_input_output_persons)
        self.average_hab.loc[self.average_hab["code"] == "P3_S13", "footprint_pers"] -= (1-CPA_O_ratio)*(waste_water_coef_persons - original_water_input_output_persons)

        #Clean 0 emission code

        self.average_bdf = self.average_bdf[self.average_bdf["footprint_pers"] > 0]
        self.average_french = self.average_french[self.average_french["footprint_pers"] > 0]
        self.average_hab = self.average_hab[self.average_hab["footprint_pers"] > 0]


"""
test=Neighborhood(time_index)
test.add_energies(energy)
test.add_activities(programme, acti_ener)
test.add_housing(housing_data)
#test.add_consumers(budget, mobility=mobility)
test.add_consumers(budget)
test.setup()
test.solve()
x = test.results
test.carbon(x)
"""  