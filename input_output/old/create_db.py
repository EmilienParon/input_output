# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 16:31:48 2020

@author: Adminlocal
"""

import sqlite3
import os
import pandas as pd
import logging
import sys

sys.path.append(os.path.dirname(__file__))
import eurostat, insee

def create_db(year=2015,emissions_source="default"):
    
  
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    
    path = os.path.abspath(os.path.dirname(__file__))
    
    # ---------------------------------
    # Activities
    branch_list = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'economy', 'metadata.xlsx'), sheet_name='branch')
    product_list = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'economy', 'metadata.xlsx'), sheet_name='product')
    
    io_product_product = eurostat.load_supply_use_data(os.path.join(path, 'data', 'eurostat', 'economy', 'Symmetric input-output table at basic prices (product by product) (naio_10_cp1700)', 'naio_10_cp1700.tsv.gz'), 2015)
    io_product_product = io_product_product[io_product_product['stk_flow'] != 'TOTAL']
    
    io_branch_product = eurostat.load_supply_use_data(os.path.join(path, 'data', 'eurostat', 'economy', 'Supply table at basic prices incl. transformation into purchasers\' prices (naio_10_cp15)', 'naio_10_cp15.tsv.gz'), 2015)
    
    technical_coefficients = eurostat.compute_technical_coefficients(io_product_product, product_list)
    
    #Add employment
    branch_jobs = eurostat.load_employment_data(os.path.join(path, 'data', 'eurostat', 'employment', 'lfsa_egan22d', "lfsa_egan22d.tsv.gz"),year)
    economy_jobs_branch_table = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'employment', 'employment_to_economy.xlsx'))
    employment_coefficients = eurostat.compute_employment_coefficients(io_branch_product, product_list, branch_list, branch_jobs, economy_jobs_branch_table)
 
    #Add surfaces
    shops_surface = insee.load_FS_commerce(os.path.join(path, 'data', 'insee', 'FS_ens_2016_2015','FS_Commerce_2016_2015.xls'),year)
    surface_coefficients = insee.compute_surface_coefficients(io_branch_product, product_list, branch_list, shops_surface)
        
    # ------------------------------
    # Final Consumption
    final_consumption = eurostat.compute_final_consumption(io_product_product, product_list)

    # Extract the final consumption of households
    households_final_consumption = final_consumption[final_consumption['output_product'] == 'P3_S14']

    # Extract the final consumption of the government and NPISH
    gov_npish_io = final_consumption[final_consumption['output_product'].isin(['P3_S15', 'P3_S13'])]

    # Make their final consumption an intermediate consumption of households
    gov_npish_prod = gov_npish_io.groupby('output_product', as_index=False)['value'].sum()
    gov_npish_prod['origin'] = 'DOM'
    gov_npish_prod['input_product'] = gov_npish_prod['output_product']
    gov_npish_prod['output_product'] = 'P3_S14'
    
    households_final_consumption = pd.concat([households_final_consumption, gov_npish_prod], sort=True)

    # Compute their IO coefficients and add them as activities in the technical coefficients dataframe
    gov_npish_io = pd.merge(gov_npish_io, gov_npish_prod[['input_product', 'value']], left_on='output_product', right_on='input_product')
    gov_npish_io['a_ij'] = gov_npish_io['value_x']/gov_npish_io['value_y']
    gov_npish_io['flow_type'] = 'consumption'
    gov_npish_tech_coef = gov_npish_io[['origin', 'flow_type', 'input_product_x', 'output_product', 'a_ij']]
    gov_npish_tech_coef.columns = ['origin', 'flow_type', 'input_product', 'output_product', 'a_ij']

    # ------------------------------
    # Capital
    # Compute the coefficients to get from products to assets
    product_asset = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'capital', 'metadata_.xlsx'), sheet_name='metadata')
    product_asset.dropna(inplace=True)
    asset_io = eurostat.compute_product_to_asset_coefficients(io_product_product, product_asset)
    
    # Load the fixed capital formation of branches
    asset_formation = eurostat.load_gfcf_data(os.path.join(path, 'data', 'eurostat', 'capital', 'Cross-classification of gross fixed capital formation by industry and by asset (flows) (nama_10_nfa_fl)', 'nama_10_nfa_fl.tsv.gz'), 2015)
    prod_40_64 = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'capital', 'metadata_.xlsx'), sheet_name='na40_na64')
    capital_formation_coefficients = eurostat.compute_capital_flows_coefficients(io_product_product, asset_formation, asset_io, prod_40_64, 'fixed_capital_formation')
    
    # Load the fixed capital consumption of branches
    assets_delta = eurostat.load_fixed_capital_delta_data(os.path.join(path, 'data', 'eurostat', 'capital', 'Cross-classification of fixed assets by industry and by asset (stocks) (nama_10_nfa_st)', 'nama_10_nfa_st.tsv.gz'), 2015)

    cfc = pd.merge(asset_formation, assets_delta, on = ['nace_r2', 'asset10'])
    cfc['value'] = cfc['gfcf'] - cfc['delta']
    cfc = cfc[['nace_r2', 'asset10', 'value']]
    
    capital_consumption_coefficients = eurostat.compute_capital_flows_coefficients(io_product_product, cfc, asset_io, prod_40_64, 'fixed_capital_consumption')


    def get_coefficients(intensity,flow_type):
        coefficients = intensity.reset_index().melt(id_vars=["product_id"])
        coefficients = coefficients.rename({"product_id":"output_product","variable":"input_product","value":"a_ij"},axis=1)
        coefficients = coefficients[coefficients.a_ij > 0]
        coefficients["origin"] = "IMP"
        coefficients["flow_type"] = "consumption"#flow_type
        coefficients = coefficients[["origin","flow_type","input_product","output_product","a_ij"]]

        return coefficients
    
    # Coefficients for energy and waste

    branch_energy = eurostat.load_energy_data(os.path.join(path, 'data', 'eurostat', 'energy', 'Energy supply and use by NACE Rev. 2 activity', "env_ac_pefasu.tsv.gz"),year)
    economy_energy_branch_table = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'energy', 'energy_to_economy.xlsx'))
    energy_intensity = eurostat.compute_energy_intensity(io_branch_product, branch_energy, product_list, branch_list, economy_energy_branch_table)
    energy_coefficients = get_coefficients(energy_intensity, "energy")
    
    branch_waste = eurostat.load_waste_data(path,year)
    economy_waste_branch_table = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'waste', 'waste_to_economy.xlsx'))
    waste_intensity = eurostat.compute_waste_intensity(io_branch_product, branch_waste, product_list, branch_list, economy_waste_branch_table)
    waste_coefficients = get_coefficients(waste_intensity, "waste")
    
    if emissions_source == "custom":
        # -------------------------------
        # Concatenate activities, capital and gov/NPISH activities, energy and waste
        technical_coefficients = pd.concat([
            technical_coefficients,
            asset_io,
            capital_formation_coefficients,
            capital_consumption_coefficients,
            gov_npish_tech_coef,
            energy_coefficients,
            waste_coefficients
        ], sort = True)
        

    branch_emissions = eurostat.load_emissions_data(os.path.join(path, 'data', 'eurostat', 'emissions', 'Air emissions accounts by NACE Rev. 2 activity (env_ac_ainah_r2)', 'env_ac_ainah_r2.tsv.gz'), year)
    
    economy_emissions_branch_table = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'emissions', 'emissions_to_economy.xlsx'))
    carbon_intensity = eurostat.compute_production_carbon_intensity(io_branch_product, branch_emissions, product_list, branch_list, economy_emissions_branch_table)
    
    def adjust_carbon_intensity(path,sector,carbon_intensity,intensity):
        emission_factors = eurostat.get_ademe_ef(path,sector,amont=False) #all emissions
        carbon_intensity = carbon_intensity.set_index("output_product") 
        if sector == "energy":             # Remove the direct emissions of the energy part
            emission_factors_direct = emission_factors.copy()
            emission_factors_direct["ef"] = emission_factors_direct["ef"].subtract(get_ademe_ef(path,sector,amont=True)["ef"]) #all emissions - amont
            emission_factors_direct = emission_factors_direct.fillna(0)
            carbon_intensity.loc[:,"carbon_intensity"] -= intensity.multiply(emission_factors_direct["ef"], axis=1).sum(axis=1) #kWh/Euros * kgCO2/kWh = kgCO2/Euros
        else: #Remove all the emissions
            carbon_intensity.loc[:,"carbon_intensity"] -= intensity.multiply(emission_factors["ef"], axis=1).sum(axis=1) #t/Euros * kgCO2/t = kgCO2/Euros (in case of waste)
        carbon_intensity = carbon_intensity.reset_index()
        
        return carbon_intensity        
    
    # Add the sector carbon intensity
        # sector_carbon_intensity = pd.DataFrame(columns=carbon_intensity.columns)
        # sector_carbon_intensity["output_product"] = intensity.index
        # sector_carbon_intensity["airpol"] = "CO2E"
        # sector_carbon_intensity["flow_type"] = sector
        # sector_carbon_intensity["carbon_intensity"] = intensity.multiply(emission_factors["ef"], axis=1).sum(axis=1).values
    
    if emissions_source == "custom":
        # carbon_intensity = adjust_carbon_intensity(path,"energy",carbon_intensity,energy_intensity)
        # carbon_intensity = adjust_carbon_intensity(path,"waste",carbon_intensity,waste_intensity)
        
        # Energy
        emission_factors = eurostat.get_ademe_ef(path,"energy",amont=False) #all emissions
        emission_factors["input_product"] = "CO2E"
        emission_factors["output_product"] = emission_factors.index.values
        emission_factors["origin"] = "biosphere"
        emission_factors["a_ij"] = emission_factors["ef"]
        emission_factors["flow_type"] = "consumption"
        energy_tech_coefs = emission_factors[["origin", "flow_type", "input_product", "output_product", "a_ij"]]
        
        
        # Waste
        emission_factors = eurostat.get_ademe_ef(path,"waste",amont=False)
        waste_emissions = eurostat.compute_emissions_from_waste(path, branch_waste)
        waste_emissions.dropna(inplace=True)
        waste_emissions["input_product"] = "CO2E"
        waste_emissions["origin"] = "biosphere"
        waste_emissions["flow_type"] = "consumption"
        waste_emissions = waste_emissions.rename({"ef":"a_ij"},axis=1)
        waste_emissions = waste_emissions.drop(["id_ademe","treatment","id_eurostat_base"],axis=1)
        
        share = pd.read_excel(os.path.join(path,"data","ademe","waste","share.xlsx")) # share of waste treatment for each waste category
        share = pd.melt(share,id_vars=['identifiant'])
        share = share.rename({"variable":"input_product", "identifiant":"output_product", "value":"a_ij"},axis=1)
        share.dropna(inplace=True)
        share["origin"] = "DOM"
        share["flow_type"] = "consumption"
        share["input_product"] = share.apply(lambda x: x.input_product + "_" + x.output_product, axis=1)
        
        eurostat_to_ademe = pd.read_excel(os.path.join(path,"data","ademe","waste","conversion.xlsx")) # share 
        eurostat_to_ademe.dropna(inplace=True)
        
        waste_coef_mapped = pd.merge(waste_coefficients, eurostat_to_ademe[["id_eurostat_base", "id_ademe", "value"]], left_on = "input_product", right_on="id_eurostat_base")
        waste_coef_mapped["a_ij"] *= waste_coef_mapped["value"]
        waste_coef_mapped = waste_coef_mapped.groupby(["origin", "flow_type", "id_ademe", "output_product"], as_index=False)["a_ij"].sum()
        waste_coef_mapped.rename({"id_ademe": "input_product"}, axis=1, inplace=True)
    
    
    households_direct_emissions = eurostat.compute_households_direct_emissions(branch_emissions)
    
    if emissions_source == "custom":
        technical_coefficients2 = pd.concat([technical_coefficients.reset_index(drop=True),share,waste_coef_mapped],ignore_index=True,sort=True)
        carbon_intensity2 = pd.concat([carbon_intensity,
                                       waste_emissions.rename({"input_product":"airpol","a_ij":"carbon_intensity"},axis=1).drop(["origin","flow_type"],axis=1),
                                       energy_tech_coefs.rename({"input_product":"airpol","a_ij":"carbon_intensity"},axis=1).drop(["origin","flow_type"],axis=1)],ignore_index=True,sort=True)
    
        prod_activities = eurostat.prepare_production_activities(technical_coefficients2, carbon_intensity2)
        row_prod_activities = eurostat.prepare_row_production_activities(technical_coefficients2, carbon_intensity2)
    else:
        prod_activities = eurostat.prepare_production_activities(technical_coefficients, carbon_intensity)
        row_prod_activities = eurostat.prepare_row_production_activities(technical_coefficients, carbon_intensity)   
        
    cons_activities = eurostat.prepare_consumption_activites(households_final_consumption, households_direct_emissions)
    activities = prod_activities + row_prod_activities + cons_activities
    
    # Write employment to csv
    employment_coefficients.to_csv(os.path.join(path, 'data', 'eurostat', 'employment.csv'))
    
    # Write surfaces to csv
    surface_coefficients.to_csv(os.path.join(path, 'data', 'insee', 'surface.csv'))
    
    # Write the results to the db
    db_filename = os.path.join(path, 'data', 'eurostat', 'eurostat_activities.db')
    connection = sqlite3.connect(db_filename)

    connection.execute("""
    DROP TABLE IF EXISTS eurostat_activities
    """)

    connection.execute("""
    CREATE TABLE IF NOT EXISTS eurostat_activities (
        id TEXT,
        service TEXT,
        territory TEXT,
        type TEXT,
        data TEXT
    )
    """)

    for activity in activities:
        connection.execute("""
        INSERT INTO eurostat_activities (id, service, territory, type, data)
        VALUES (?, ?, ?, ?, ?)
        """, (activity['id'], activity['service'], activity['territory'], activity['type'], activity['data']))
        connection.commit()

    connection.close()

# year = 2015
# path = os.path.abspath(os.path.dirname(__file__))
# branch_list = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'economy', 'metadata.xlsx'), sheet_name='branch')
# product_list = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'economy', 'metadata.xlsx'), sheet_name='product')
    
# branch_energy = load_energy_data(os.path.join(path, 'data', 'eurostat', 'energy', 'Energy supply and use by NACE Rev. 2 activity', "env_ac_pefasu.tsv.gz"),year)
# io_product_product = load_supply_use_product_product_data(os.path.join(path, 'data', 'eurostat', 'economy', 'Symmetric input-output table at basic prices (product by product) (naio_10_cp1700)', 'naio_10_cp1700.tsv.gz'), year)
# io_branch_product = load_supply_use_branch_product_data(os.path.join(path, 'data', 'eurostat', 'economy', 'Supply table at basic prices incl. transformation into purchasers\' prices (naio_10_cp15)', 'naio_10_cp15.tsv.gz'), year)
# economy_energy_branch_table = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'energy', 'energy_to_economy.xlsx'))
# energy_intensity = compute_energy_intensity(io_branch_product, branch_energy, product_list, branch_list, economy_energy_branch_table)