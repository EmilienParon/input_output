import logging
import os
import pandas as pd
import numpy as np


def allocate_branch_flows_to_products(flows, sut_branch_product, by=None):
    
    branch_production = sut_branch_product.groupby("activity", as_index=False)["volume"].sum()
        
    # Divide the value of the flows by the production of the branches
    coef = pd.merge(flows, branch_production, on="activity")
    coef["k"] = coef["volume_x"]/coef["volume_y"]
    # coef["induse_to_prod_na"] = "CPA_" + coef["induse"]
    
    if by is None:
        coef["var"] = "var"
        by_variable = ["var"]
    else:
        if isinstance(by, list):
            by_variable = by
        else:
            by_variable = [by]
            
    
    flows_per_product = pd.merge(
        sut_branch_product[["activity", "product", "volume"]],
        coef[["activity", "k"] + by_variable],
        on = "activity"
    )
    
    flows_per_product["volume"] = flows_per_product["k"]*flows_per_product["volume"]
    
    if "product" in by_variable:
        by_variable = [x.replace("product", "product_y") for x in by_variable]
    
    flows_per_product = flows_per_product.groupby(["product_x"] + by_variable, as_index=False)["volume"].sum()
    
    
    if by is None:
        result = flows_per_product[["product_x", "volume"]]
    else:
        result = flows_per_product[["product_x", "volume"] + by_variable]
        
    result.rename(columns={"product_x": "activity", "product_y": "product"}, inplace=True)
    
    result["activity"] = result["activity"] + "_production_activity"
    
    return result


