from .monetary_flows import load_supply_use_flows
from .physical_flows import load_emissions_flows, load_energy_flows, load_waste_flows
from .monetary_coefficients import compute_product_coefficients
from .physical_coefficients import compute_emissions_coefficients, compute_energy_coefficients, compute_waste_coefficients

from scipy.optimize import minimize, Bounds, LinearConstraint, basinhopping, differential_evolution, dual_annealing
from scipy.sparse import coo_matrix


def compute_technical_coefficients(detail_energy, detail_waste):
    """
        Load and format all Eurostat monetary flows (in M€). Inputs : intermediate consumption, formation and consumption of fixed capital. Outputs : production value.
        Load and format Eurostat emissions flows (in tCO2e).
        If needed, load Eurostat energy flows (in TJ, only "emissions relevant" energy flows).
        If needed, load Eurostat waste flows (in t).
    
    Args:
        detail_energy (bool): use eurostat energy flows + ADEME emissions factors (True) instead of eurostat emissions factors (False) 
        detail_waste (bool): use eurostat waste flows + ADEME emissions factors + ADEME shares of treatment for each type of waste (True) instead of eurostat emissions factors (False) 
    """

    # ---------------------------------------
    # Load all flows
    # Supply-use monetary flows in euros
    sut_product_product = load_supply_use_flows("product_product")
    sut_branch_product = load_supply_use_flows("branch_product")

    # Capital formation and consumption in euros
    capital_formation = load_capital_formation_flows()
    capital_consumption = load_capital_consumption_flows(capital_formation)
    
    # Estimate capital flows at nace 64 level (instead of nace 40)
    capital_formation = allocate_capital_flows_to_nace_64(capital_formation, sut_branch_product)
    capital_consumption = allocate_capital_flows_to_nace_64(capital_consumption, sut_branch_product)
    
    # Allocate capital flows from branches (nace) to products (CPA)
    capital_formation = allocate_branch_flows_to_products(capital_formation, sut_branch_product, by = "asset10")
    capital_consumption = allocate_branch_flows_to_products(capital_consumption, sut_branch_product, by = "asset10")
    
    # Allocate capital flows from assets (asset10) to products (CPA)
    capital_formation = allocate_assets_to_products(capital_formation, sut_product_product, by_origin=True)
    capital_consumption = allocate_assets_to_products(capital_consumption, sut_product_product, by_origin=False)
    
    # Final consumption in euros
    # households_consumption = load_households_consumption_flows()

    # Emissions flows in kgCO2e
    emissions = load_emissions_flows()
    emissions_products = allocate_branch_flows_to_products(emissions, sut_branch_product, by = ["origin", "airpol"])    
    emissions_households = extract_households_emissions(emissions)

    # Energy flows
    if detail_energy:
        energy = load_energy_flows()
        energy_products = allocate_branch_flows_to_products(energy, sut_branch_product, by = ["origin", "prod_nrg"])
        energy_households = extract_households_energy(energy)
        
        # ADEME energy emission factors
        ademe_ef_energy = load_ademe_emissions_factors()
        
        # Custom energy emission factors (biogas)
        custom_ef_energy = load_custom_emissions_factors()
        
        ef_energy = pd.concat([ademe_ef_energy, custom_ef_energy], sort=True)

    
    
    # ---------------------------------------
    # Compute the input - output coefficients of the activities
    total_product_production = sut_product_product[sut_product_product.prod_na == "P1"].groupby("induse", as_index=False)["value"].sum()
    
    monetary_coefficients = compute_coefficients(
        sut_product_product[(sut_product_product.induse != "P51G") & (sut_product_product.prod_na != "P1")],
        output_var="induse",
        input_var="prod_na",
        origin_var="stk_flow",
        flow_type="consumption",
        reference_output_flows=total_product_production
    )
    
    capital_formation_coefficients = compute_coefficients(
        capital_formation,
        output_var="induse",
        input_var="prod_na",
        origin_var="stk_flow",
        flow_type="capital_formation",
        reference_output_flows = total_product_production
    )
    
    capital_consumption_coefficients = compute_coefficients(
        capital_consumption,
        output_var="induse",
        input_var="prod_na",
        origin_var="stk_flow",
        flow_type="capital_consumption",
        reference_output_flows = total_product_production
    )
    
    emissions_coefficients = compute_coefficients(
        emissions_products,
        output_var="induse",
        input_var="airpol",
        origin_var="origin",
        flow_type="consumption",
        reference_output_flows = total_product_production
    )
    
    energy_coefficients = compute_coefficients(
        energy_products,
        output_var="induse",
        input_var="prod_nrg",
        origin_var="origin",
        flow_type="consumption",
        reference_output_flows = total_product_production
    )
    
    ademe_coefficients = compute_coefficients(
        ademe_ef_energy,
        output_var="prod_nrg",
        input_var="airpol",
        origin_var="origin",
        flow_type="consumption"
    )
    
    
    def rss(k, ref_df):
        
        ref_df["k"] = k
        
        energy_emissions = pd.merge(energy_delta, ef_energy[["prod_nrg", "airpol", "value"]], on="prod_nrg")       
            
        
        energy_emissions = energy_emissions[energy_emissions.prod_nrg.str.contains("ER_USE")].groupby(["induse", "airpol"], as_index=False)["emissions"].sum()
            
        comp = pd.merge(emissions[["induse", "airpol", "value"]], energy_emissions[["induse", "airpol", "emissions"]], on=["induse", "airpol"])
        comp["delta"] = comp["emissions"] - comp["value"]
        comp["delta_pos"] = np.where(comp["delta"] > 0.0, comp["delta"], 0.0)
        
        res = np.sum(comp["delta_pos"]*comp["delta_pos"]) + np.sum(np.power(energy_delta.groupby("prod_nrg")["energy_corr"].sum() - energy_delta.groupby("prod_nrg")["value"].sum(), 2))
        print(res)
        
        return res
    
    df = pd.merge(energy[energy.prod_nrg.str.contains("ER_USE")], ef_energy[["prod_nrg", "airpol", "value"]], on="prod_nrg") 
    df = pd.merge(emissions[["induse", "airpol", "value"]], energy_emissions[["induse", "airpol", "emissions"]], on=["induse", "airpol"])
    
    ref_df = energy[energy.prod_nrg.str.contains("ER_USE")][["induse", "prod_nrg"]]
    
    
    
    
    
    

    
    energy_use_by_branch = energy[energy.prod_nrg.str.contains("ER_USE")].copy()
    total_energy_use = energy[energy.prod_nrg.str.contains("ER_USE")].groupby("prod_nrg")["value"].sum()
    
    energy_use_by_branch = pd.merge(energy_use_by_branch, total_energy_use, on=["prod_nrg"])
    energy_use_by_branch["k"] = energy_use_by_branch["value_x"]/energy_use_by_branch["value_y"]
    
        
    induse_index = pd.DataFrame({"induse": energy_use_by_branch.induse.unique()})
    induse_index["induse_index"] = np.arange(0, induse_index.shape[0])
    
    prod_nrg_index = pd.DataFrame({"prod_nrg": energy_use_by_branch.prod_nrg.unique()})
    prod_nrg_index["prod_nrg_index"] = np.arange(0, prod_nrg_index.prod_nrg.shape[0])
    
    energy_use_by_branch = pd.merge(energy_use_by_branch, induse_index, on="induse")
    energy_use_by_branch = pd.merge(energy_use_by_branch, prod_nrg_index, on="prod_nrg")
    
    
    total_energy_use = pd.merge(total_energy_use, prod_nrg_index, on="prod_nrg")
    
    
    A_br = coo_matrix(
        (energy_use_by_branch.k, (energy_use_by_branch.induse_index, energy_use_by_branch.prod_nrg_index)),
        shape=(induse_index.shape[0], prod_nrg_index.shape[0])
    ).toarray()
    
    en = total_energy_use.sort_values("prod_nrg_index")["value"].values
    en = np.diag(en)
    
    Ene_br_ref = np.dot(A_br, en)
    
    
    A_constr = [(i, j) for i in np.arange(0, prod_nrg_index.shape[0]) for j in np.arange(0, prod_nrg_index.shape[0]*induse_index.shape[0])]
    A_constr = np.array(A_constr)
    
    N = induse_index.shape[0]
         
    
    A_constr = coo_matrix(
        (np.zeros(A_constr.shape[0]), (A_constr[:, 0], A_constr[:, 1])),
        shape=(A_constr[:, 0].max()+1, A_constr[:, 1].max()+1)
    ).toarray()
    
    for i in np.arange(0, prod_nrg_index.shape[0]):
        for j in np.arange(0, prod_nrg_index.shape[0]*induse_index.shape[0]):
            if j >= i*N and j < (i+1)*N:
                A_constr[i, j] = 1.0
    
    
    
    
    emission_factors = ef_energy[ef_energy.prod_nrg.str.contains("ER_USE")][["prod_nrg", "airpol", "value"]]
    
    airpol_index = pd.DataFrame({"airpol": ef_energy.airpol.unique()})
    airpol_index["airpol_index"] = np.arange(0, airpol_index.shape[0])
    
    emission_factors = pd.merge(emission_factors, airpol_index, on="airpol")
    emission_factors = pd.merge(emission_factors, prod_nrg_index, on="prod_nrg")
    
    FE = coo_matrix(
        (emission_factors.value, (emission_factors.prod_nrg_index, emission_factors.airpol_index)),
        shape=(prod_nrg_index.shape[0], airpol_index.shape[0])
    ).toarray()
    
    ref_emissions = emissions[emissions.airpol.isin(airpol_index.airpol.unique())]
        
    ref_emissions = pd.merge(ref_emissions, induse_index, on="induse")
    ref_emissions = pd.merge(ref_emissions, airpol_index, on="airpol")
        
    Emi_br_ref = coo_matrix(
        (ref_emissions.value, (ref_emissions.induse_index, ref_emissions.airpol_index)),
        shape=(induse_index.shape[0], airpol_index.shape[0])
    ).toarray()
        

    
    
    def correction(k):
        
        corr = [(i, j, 0.0) for i in np.arange(0, induse_index.shape[0]) for j in np.arange(0, prod_nrg_index.shape[0])]
        corr = np.array(corr)
        
        A_corr = coo_matrix(
            (k, (corr[:, 0], corr[:, 1])),
            shape=(induse_index.shape[0], prod_nrg_index.shape[0])
        ).toarray()
        
        A = A_br + A_corr
        
        # neg_coeffs = np.abs(np.where(A < 0.0, A, 0)).sum()
        sum_to_one = np.abs(A.sum(axis=0) - 1.0).sum()
        # non_zero_coeffs = np.where(np.abs(A) > 0.0, 1.0, 0).sum()
        
        Ene_br = np.dot(A, en)
        Emi_br = np.dot(Ene_br, FE)
        
        delta_emi = Emi_br - Emi_br_ref
        delta_emi_pos = np.where(delta_emi > 0.0, delta_emi, 0.0)
        
        rss = (delta_emi_pos*delta_emi_pos).sum()/1e10 + 1e12*sum_to_one
        
        print(rss)
        
        return rss

    correction(np.zeros(832))
    
    a_ij = A_br.reshape((1, 64*13))[0]

    x = minimize(
        correction,
        np.zeros(832),
        bounds=Bounds(-a_ij, 1-a_ij)
    )
    
    k = x["x"]
    

    comp.to_csv("comp.csv")
    

    return technical_coefficients


def build_database(detail_energy = False, detail_waste = False):
    """
        Load and format all source datasets (Eurostat) containing the inventory flows of activities and services (= products). These flows can be in euros, kgCO2e, kWh, kg...
        Compute the technical coefficients of each activity : the value of the input flows necessary for the production of one unit of the output flow.
        Store the result as a table of actvities in a SQLite database.
    
    Args:
        detail_energy (bool): use eurostat energy flows + ADEME emissions factors (True) instead of eurostat emissions factors (False) 
        detail_waste (bool): use eurostat waste flows + ADEME emissions factors + ADEME shares of treatment for each type of waste (True) instead of eurostat emissions factors (False) 
    """

    technical_coefficients = compute_technical_coefficients(detail_energy, detail_waste)


