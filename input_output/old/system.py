import pandas as pd
import numpy as np

from oemof.solph import Model, EnergySystem, Flow, Bus, Source, Sink, Transformer
from oemof.solph.components import GenericStorage
from oemof.solph.custom import Link
from oemof.outputlib import views
from oemof.outputlib.processing import create_dataframe


class Territory:
    
    def __init__(self, name, slack=False, slack_cost=None):
        self.name = name
        self.slack = slack
        self.slack_cost = slack_cost
        self.transport_costs = {}
        self.activities = {}
        self.services = {}
        


class System:

    def __init__(self, time_index):
        self.io = EnergySystem(timeindex=time_index)
        self.territories = {}
#        self.sources = []
#        self.sinks = []
#        self.links = []
#        self.activities = []


    def add_territories(self, territories):
        for t in territories:
            self.territories[t.name] = t

    def set_transport_costs(self, territory_1, territory_2, cost):
        self.territories[territory_1.name].transport_costs[territory_2.name] = cost
        self.territories[territory_2.name].transport_costs[territory_1.name] = cost


    def get_node(self, service, territory):
        """
            Get a node given its territory and service in the nodes dictionnary
            (create one if not already existing)
        """
        if territory == 'biosphere':

            if service not in self.nodes['biosphere']['services'].keys():
                node = self.add_biosphere_node(service)

            return self.nodes['biosphere']['services'][service]

        else:

            for t in self.nodes['territories'].keys():

                if service not in self.nodes['territories'][t]['services'].keys():
                    #print("creating bus for node " + service + ' ' + t)
                    node = Bus(label = t + '_' + service + '_node')
                    self.nodes['territories'][t]['services'][service] = node
                    self.io.add(node)

            return self.nodes['territories'][territory]['services'][service]

    def add_biosphere_node(self, service):

        # Create the biosphere account and add it to the system
        node = Bus(label = 'biosphere_' + service + '_node')
        self.nodes['biosphere']['services'][service] = node

        # Add infinite sources and sinks to the account
        # to balance supply and demand in case of local surplus or shortfalls
        source = Source(
            label = 'biosphere_' + service + '_source',
            outputs = { node: Flow(variable_costs = 0.0, nominal_value = 1e12) }
        )

        sink = Sink(
            label = 'biosphere_' + service + '_sink',
            inputs = { node: Flow(variable_costs = 0.0, nominal_value = 1e12) }
        )

        # Add the elements to the oemof system object
        self.io.add(
            node,
            source,
            sink
        )

        return node

    def setup_market_sources_and_sinks(self):

        for territory in self.nodes['territories'].keys():
            services = self.nodes['territories'][territory]['services']

            for service in services:

                market = self.get_node(service, territory)

                # Add infinite sources and sinks to markets that must be balanced (slack = True)
                # to balance supply and demand of all markets in case of local surplus or shortfalls
                if territory + '_' + service + '_source' not in self.sources and self.nodes['territories'][territory]['slack']['has_slack'] == True:
                    market = self.get_node(service, territory)
                    source = Source(
                        label = territory + '_' + service + '_source',
                        outputs = { market: Flow(variable_costs = self.nodes['territories'][territory]['slack']['source_costs'], nominal_value = 1e9) }
                    )
                    self.sources.append(territory + '_' + service + '_source')
                    self.io.add(source)

                if territory + '_' + service + '_sink' not in self.sinks and self.nodes['territories'][territory]['slack']['has_slack'] == True:
                    market = self.get_node(service, territory)
                    sink = Sink(
                        label = territory + '_' + service + '_sink',
                        inputs = { market: Flow(variable_costs = self.nodes['territories'][territory]['slack']['sink_costs'], nominal_value = 1e9) }
                    )
                    self.sinks.append(territory + '_' + service + '_sink')
                    self.io.add(sink)


    def setup_market_links(self):

        for territory in self.nodes['territories'].keys():
            services = self.nodes['territories'][territory]['services']

            for service in services:

                market = self.get_node(service, territory)

                for t in self.nodes['territories'].keys():
                    if t != territory:

                        ext_market = self.get_node(service, t)

                        if territory + '_to_' + t + '_' + service + '_link' not in self.links:
                            #print("creating " + service + " link between " + territory + " and " + t)
                            exchange_costs = self.nodes['territories'][territory]['exchange_costs'][t]
                            out_link = Transformer(
                                label = territory + '_to_' + t + '_' + service + '_link',
                                inputs = { market: Flow(variable_costs = exchange_costs, nominal_value = 1e12) },
                                outputs = { ext_market: Flow(variable_costs = exchange_costs, nominal_value = 1e12) },
                                conversion_factors = { (market, ext_market): 1.0 }
                            )
                            self.links.append(territory + '_to_' + t + '_' + service + '_link')
                            self.io.add(out_link)


    def add_activities(self, activities):

        for activity in activities:

            inputs = {}
            outputs = {}
            conversion_factors = {}

            # Find the reference flow which will be used to scale all other flows
            services = activity.input_services + activity.output_services

            for service in services:
                if service.capacity is not None:
                    ref_capacity = service.capacity
                    ref_conv_factor = service.conversion_factor

            # Find which nodes are both inputs and outputs (= self consumption)
            # if there are any, remove the input flow
            # and substract their conversion_factor to the output conversion_factor
            remove_list = []

            for out in activity.output_services:
                for inp in activity.input_services:
                    if out.label == inp.label and out.destination_territory == inp.origin_territory:
                        out.conversion_factor -= inp.conversion_factor
                        remove_list.append(inp)

            act_inputs = []
            for inp in activity.input_services:
                if len(remove_list) > 0:
                    for flow in remove_list:
                        if flow.label != inp.label or flow.origin_territory != inp.origin_territory:
                            act_inputs.append(inp)
                else:
                    act_inputs.append(inp)

            # Create all input flows
            for inp in act_inputs:


                if inp.flow_type == 'consumption':

                    node = self.get_node(inp.label, inp.origin_territory)
                    capacity = np.max(ref_capacity)*inp.conversion_factor/ref_conv_factor
                    profile = capacity/np.max(capacity)

                    inputs[ node ] = Flow(
                        variable_costs = 0.0,
                        nominal_value = capacity,
                        actual_value = profile,
                        fixed = activity.fixed
                    )

                    conversion_factors[ node ] = inp.conversion_factor


                # Create capital use flows
                elif inp.flow_type == 'capital_formation':
                    ext_capital_node = self.get_node(inp.label, inp.origin_territory)
                    activity_capital_node = self.get_node(activity.label + '_' + inp.label, inp.origin_territory)
                    activity_capital_formation_node = self.get_node(activity.label + '_' + inp.label + '_capital_formation', inp.origin_territory)

                    # Capital formation flow
                    capital_formation = np.max(ref_capacity)*inp.conversion_factor/ref_conv_factor
                    capital_formation_profile = capital_formation/np.max(capital_formation)
                    
                    if activity_capital_formation_node not in inputs.keys():

                        # Create a storage object that can store capital formation
                        # and redistribute it in the next years
                        storage = GenericStorage(
                            label = activity.label + '_' + inp.label + '_capital_storage_' + activity.territory,
                            inputs = {
                                activity_capital_node: Flow(
                                    variable_costs = 0.0,
                                    nominal_value = 1e12,
                                    fixed = activity.fixed
                                )
                            },
                            outputs = {
                                activity_capital_node: Flow(
                                    variable_costs = 0.0,
                                    nominal_value = 1e12,
                                    fixed = activity.fixed
                                )
                            },
                            nominal_storage_capacity = 1e20,
                            initial_storage_level = 0.5,
                            balanced = False
                        )
    
                        self.activities.append(storage)
                        self.io.add(storage)
    
                        # Create a capital formation activity that feeds the storage
                        capital_formation_activity = Transformer(
                            label = activity.label + '_' + inp.label + '_capital_formation_activity_' + activity.territory,
                            inputs = {
                                ext_capital_node: Flow(
                                    variable_costs = 0.0
                                )
                            },
                            outputs = {
                                activity_capital_formation_node: Flow(
                                    variable_costs = 0.0,
                                    nominal_value = 1e12,
                                    fixed = activity.fixed
                                ),
                                activity_capital_node: Flow(
                                    variable_costs = 0.0,
                                    nominal_value = 1e12,
                                    fixed = activity.fixed
                                )
                            },
                            conversion_factors = {
                                ext_capital_node: 1.0,
                                activity_capital_node: 1.0,
                                activity_capital_formation_node: 1.0
                            }
                        )
    
                        self.activities.append(capital_formation_activity)
                        self.io.add(capital_formation_activity)

                    # Add a capital formation flow as input for the activity
                    inputs[ activity_capital_formation_node ] = Flow(
                        variable_costs = 0.0,
                        nominal_value = capital_formation,
                        actual_value = capital_formation_profile,
                        fixed = activity.fixed
                    )
                    
                    conversion_factors[ activity_capital_formation_node ] = inp.conversion_factor


                    
                
            for internal in activity.internal_services:
                
                if internal.flow_type == "capital_consumption":

                    activity_capital_node = self.get_node(activity.label + '_' + internal.label, activity.territory)

                    # Capital use flow
                    capital_use = np.max(ref_capacity)*inp.conversion_factor/ref_conv_factor
                    capital_use_profile = capital_use/np.max(capital_use)

                    # Add a capital use flow as input for the activity
                    inputs[ activity_capital_node ] = Flow(
                        variable_costs = 0.0,
                        nominal_value = capital_use,
                        actual_value = capital_use_profile,
                        fixed = activity.fixed
                    )

                    conversion_factors[ activity_capital_node ] = internal.conversion_factor
                


            # Create all output flows
            for out in activity.output_services:

                node = self.get_node(out.label, out.destination_territory)
                capacity = np.max(ref_capacity)*out.conversion_factor/ref_conv_factor
                profile = capacity/np.max(capacity)

                outputs[ node ] = Flow(
                    variable_costs = 0.0,
                    nominal_value = capacity,
                    actual_value = profile,
                    fixed = activity.fixed
                )

                conversion_factors[ node ] = out.conversion_factor

            # Create the activity
            activity = Transformer(
                label = activity.label,
                inputs = inputs,
                outputs = outputs,
                conversion_factors = conversion_factors
            )

            self.activities.append(activity)
            self.io.add(activity)



    def balance(self):
        # create optimization model
        optimization_model = Model(energysystem = self.io)

        print('Solving system')

        # solve problem
        optimization_model.solve(
            solver = 'cbc',
            solver_io = 'lp',
            solve_kwargs = {'tee': True, 'keepfiles': True}
        )

        print('Extracting results')

        df = create_dataframe(optimization_model)
        df = df[df.variable_name == 'flow'].copy()
        df['from'] = df['pyomo_tuple'].apply(lambda x: x[2][0].label)
        df['to'] = df['pyomo_tuple'].apply(lambda x: x[2][1].label)
        
        self.results = df[['from', 'to', 'timestep', 'value']]




