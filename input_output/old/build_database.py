import sqlite3
import os
import pandas as pd

from .monetary_flows import load_supply_use_flows
from .physical_flows import load_emissions_flows, load_energy_flows, load_waste_flows
from .monetary_coefficients import compute_product_coefficients, allocate_branch_flows_to_products
from .physical_coefficients import compute_emissions_coefficients, compute_energy_coefficients, compute_waste_coefficients

def prepare_technical_coefficients():

    # ---------------------------------------
    # Load all flows
    # Supply-use monetary flows in euros
    sut_product_product = load_supply_use_flows("product_product")
    sut_branch_product = load_supply_use_flows("branch_product")
    
    # Production output flows
    production_output = load_production_output(sut_product_product)
    
    # Capital formation and consumption in euros
    capital_formation = load_capital_formation_flows()
    capital_consumption = load_capital_consumption_flows(capital_formation)
    
    # Estimate capital flows at nace 64 level (instead of nace 40)
    capital_formation = allocate_capital_flows_to_nace_64(capital_formation, sut_branch_product)
    capital_consumption = allocate_capital_flows_to_nace_64(capital_consumption, sut_branch_product)
    
    # Allocate capital flows from branches (nace) to single product activities (CPA)
    capital_formation = allocate_branch_flows_to_products(capital_formation, sut_branch_product, by = ["from", "to", "product"])
    capital_consumption = allocate_branch_flows_to_products(capital_consumption, sut_branch_product, by = ["from", "to", "product"])
    
    # Allocate capital flows from assets (asset10) to products (CPA)
    capital_formation = allocate_assets_to_products(capital_formation, sut_product_product, by_origin=True)
    capital_consumption = allocate_assets_to_products(capital_consumption, sut_product_product, by_origin=False)
    
    # Final consumption in euros/capita
    households_consumption = load_households_consumption_flows(sut_product_product)

    # Emissions flows in kgCO2e
    emissions = load_emissions_flows()
    
    emissions_products = allocate_branch_flows_to_products(emissions, sut_branch_product, by = ["from", "to", "product"]) 
    emissions_households = extract_households_emissions(emissions) 
    
    # ---------------------------------------
    # Compute the input - output coefficients of the activities
    total_product_production = sut_product_product[sut_product_product["product"] == "P1"].groupby("activity", as_index=False)["volume"].sum()
    
    output_coefficients = compute_coefficients(
        production_output,
        ref_variable="activity",
        ref_flows=total_product_production
    )
    
    output_coefficients["sub_activity"] = "production"
    
    monetary_coefficients = compute_coefficients(
        sut_product_product[(sut_product_product["activity"] != "P51G_production_activity") & (sut_product_product["product"] != "P1")],
        ref_variable="activity",
        ref_flows=total_product_production
    )
    
    monetary_coefficients["sub_activity"] = "production"
    
    capital_formation_coefficients = compute_coefficients(
        capital_formation,
        ref_variable="activity",
        ref_flows=total_product_production
    )
    
    capital_formation_coefficients["sub_activity"] = "capital_formation"
    
    capital_consumption_coefficients = compute_coefficients(
        capital_consumption,
        ref_variable="activity",
        ref_flows=total_product_production
    )
    
    capital_consumption_coefficients["sub_activity"] = "capital_consumption"
    
    emissions_coefficients = compute_coefficients(
        emissions_products,
        ref_variable="activity",
        ref_flows=total_product_production
    )
    
    emissions_coefficients["sub_activity"] = "production"
    
    
    
    households_monetary_inputs_coefficients = compute_coefficients(
        households_consumption[households_consumption["activity"] == "households_activity"],
        ref_variable="product",
        ref_flows=pd.DataFrame({
            "product": households_consumption.loc[households_consumption["activity"] == "households_activity", "product"].unique(),
            "volume": 66.4e6
        })
    )
    
    households_monetary_inputs_coefficients["sub_activity"] = "production"
    
    gov_npish_input_coefficients = compute_coefficients(
        households_consumption[households_consumption["activity"] != "households_activity"],
        ref_variable="activity",
        ref_flows=pd.DataFrame({
            "activity": households_consumption.loc[households_consumption["activity"] != "households_activity", "activity"].unique(),
            "volume": households_consumption.loc[households_consumption["activity"] != "households_activity"].groupby("activity")["volume"].sum().values
        })
    )
    
    gov_npish_input_coefficients["sub_activity"] = "production"
    
    gov_npish_output_coefficients = pd.DataFrame({
        "activity": ["P3_S13_production_activity", "P3_S15_production_activity"],
        "from": "activity",
        "to": "france",
        "product": ["P3_S13", "P3_S15"],
        "coefficient": 1.0
    })
    
    gov_npish_output_coefficients["sub_activity"] = "production"
    
    households_emitting_activities_input_coefficients = compute_coefficients(
        emissions_households,
        ref_variable="activity",
        ref_flows=pd.DataFrame({"activity": ["HH_HEAT", "HH_TRA", "HH_OTH"], "volume": 66.4e6})
    )
    
    households_emitting_activities_input_coefficients["sub_activity"] = "other"
    
    households_emitting_activities_output_coefficients = pd.DataFrame({
        "activity": ["HH_HEAT", "HH_TRA", "HH_OTH"],
        "from": "activity",
        "to": "activity",
        "product": ["HH_HEAT", "HH_TRA", "HH_OTH"],
        "coefficient": 1.0
    })
    
    households_emitting_activities_output_coefficients["sub_activity"] = "other"
    
    households_emitting_activities_consumption_coefficients = pd.DataFrame({
        "activity": "households_activity",
        "from": "france",
        "to": "activity",
        "product": ["HH_HEAT", "HH_TRA", "HH_OTH"],
        "coefficient": 1.0
    })
    
    households_emitting_activities_consumption_coefficients["sub_activity"] = "production"
    
    economy_coefficients = pd.concat([
        output_coefficients,
        monetary_coefficients,
        capital_formation_coefficients,
        capital_consumption_coefficients,
        gov_npish_input_coefficients,
        gov_npish_output_coefficients,
        emissions_coefficients
    ], sort=True)
    
    economy_coefficients["territory"] = "france"
    
    
    row_economy_coefficients = economy_coefficients.copy()
    row_economy_coefficients["territory"] = "rest_of_the_world"
    row_economy_coefficients.loc[row_economy_coefficients["from"] == "france", "from"] = "rest_of_the_world"
    row_economy_coefficients.loc[row_economy_coefficients["to"] == "france", "to"] = "rest_of_the_world"
    row_economy_coefficients = row_economy_coefficients.groupby([c for c in row_economy_coefficients.columns if c != "coefficient"], as_index=False)["coefficient"].sum()
    row_economy_coefficients.loc[(row_economy_coefficients["from"] == "biosphere") & (row_economy_coefficients["activity"] == "CPA_D_production_activity"), "coefficient"] *= 5.0
    row_economy_coefficients.loc[(row_economy_coefficients["from"] == "biosphere"), "coefficient"] *= 2.0

    households_coefficients = pd.concat([
        households_monetary_inputs_coefficients,
        households_emitting_activities_input_coefficients,
        households_emitting_activities_output_coefficients,
        households_emitting_activities_consumption_coefficients
    ], sort=True)
    
    households_coefficients["territory"] = "france"
    
    
    coefficients = pd.concat([
        economy_coefficients,
        row_economy_coefficients,
        households_coefficients],
    sort=True)

    return coefficients


def build_database():

    technical_coefficients = prepare_technical_coefficients()
    
    path = os.path.abspath(os.path.dirname(__file__))
    db_filename = os.path.join(path, 'data', 'activities.db')
    connection = sqlite3.connect(db_filename)
    
    connection.execute("""
        DROP TABLE IF EXISTS activities
    """)
    
    technical_coefficients.to_sql(name="activities", con=connection)
    
    connection.close()


