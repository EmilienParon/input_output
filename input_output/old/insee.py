# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 14:09:26 2020

@author: Giuseppe Peronato
"""

import pandas as pd
import numpy as np
import os
import scipy.stats
import logging


def load_FS_commerce(path,year):
    # path = r"D:\DATA\g.peronato\dev\quartier_epcm\input_output\input_output\data\insee\FS_ens_2016_2015\FS_Commerce_2016_2015.xls"
    # year = 2015
    
    sheets = pd.read_excel(path, sheet_name=None,header=None)

    if year == 2016:
        y = 0
    elif year == 2015:
        y = 3
    else:
        raise "Only years 2015 or 2016"
        
    
    data = []
    for key in sheets.keys():
        sheet = sheets[key]
        sector = sheet.iloc[0,0]
        if not pd.isnull(sector):
            data.append({"sector_id": int(sector[17:20]),
                         "sector_label":sector.split("(")[-1].rstrip(")"),
                        "floor_area": pd.to_numeric(sheet.loc[sheet[0] == "Surface totale de vente (en m²)",1+y],errors="coerce").values[0],
                         "profit": pd.to_numeric(sheet.loc[sheet[0] == "Marge commerciale (en millions d'euros)",1+y],errors="coerce").values[0],
                         "jobs": pd.to_numeric(sheet.loc[sheet[0] == "Effectifs équivalent temps plein (ETP) sur l'exercice comptable",1+y],errors="coerce").values[0]})
                         
    data = pd.DataFrame(data,columns=data[0].keys())
    
    data["branch_id"] = ""
    data.loc[(data.sector_id >=450) & (data.sector_id <460),"branch_id"] = "G45"
    data.loc[(data.sector_id >=460) & (data.sector_id <470),"branch_id"] = "G46"
    data.loc[(data.sector_id >=470) & (data.sector_id <480),"branch_id"] = "G47"
    
    return data

def modelSurface(shops_surface):
    #Model G47 floor area when missing
    linregress = scipy.stats.linregress(shops_surface.loc[shops_surface.branch_id == "G47",["profit","floor_area"]].dropna().values)
    missing = shops_surface.loc[(shops_surface.branch_id == "G47") & shops_surface.floor_area.isnull(),["profit","floor_area"]]
    modeled = missing.copy()
    modeled.floor_area = linregress.slope * missing.profit + linregress.intercept
    shops_surface.loc[(shops_surface.branch_id == "G47") & shops_surface.floor_area.isnull(),["profit","floor_area"]] = modeled
    return shops_surface

def compute_surface_coefficients(io_branch_product, product_list, branch_list, shops_surface):
    logging.info('Computing euros per surface')
    
    G47_surface = shops_surface[shops_surface.branch_id == "G47"]
    G47_surface = modelSurface(G47_surface) # model missing floor area
    
    br_prod = io_branch_product[(io_branch_product['induse'].isin(branch_list['branch_id']) & (io_branch_product['prod_na'].isin(product_list['product_id'])))].copy()
    
    # Total production of the branch
    p_branch = br_prod.groupby(['induse'], as_index=False)['value'].sum()
    p_branch.columns = ['induse', 'p_j']
    
    # Total production of products
    p_prod = br_prod.groupby(['prod_na'], as_index=False)['value'].sum()
    p_prod.columns = ['prod_na', 'p_i']
    
    # Branch > product repartition coefficients and matrix
    a = br_prod.groupby(['induse', 'prod_na'], as_index=False)['value'].sum()
    a = pd.merge(a, p_branch, on = 'induse')
    a['a_ij'] = a['value']/a['p_j']
    
    #Scale profit of the INSEE values so that it matches the one of the EUROSTAT data
    scaling_factor = p_branch.loc[p_branch.induse == "G47","p_j"].values[0]/G47_surface["profit"].sum()
    G47_surface.loc[:,"profit_scaled"] = G47_surface["profit"]* scaling_factor
    
    a = pd.merge(a, branch_list[['branch_id', 'branch_index']], left_on = 'induse', right_on = 'branch_id')
    a = pd.merge(a, product_list[['product_id', 'product_index']], left_on = 'prod_na', right_on = 'product_id')
    
    a = pd.merge(a,G47_surface,left_on="branch_id",right_on="branch_id",how="inner")
    
    a = a[a.value > 0] #Only products with production from branch G47
    a["a_ij"] = a["profit_scaled"]/a["floor_area"]
    
    surface_coefficients = a[["prod_na","sector_id","a_ij"]]
    surface_coefficients = surface_coefficients.set_index("sector_id")
    return surface_coefficients

if __name__ == "__main__":
    import eurostat
    path = os.path.abspath(os.path.dirname(__file__))
    year = 2015
    
    # ---------------------------------
    # Activities
    branch_list = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'economy', 'metadata.xlsx'), sheet_name='branch')
    product_list = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'economy', 'metadata.xlsx'), sheet_name='product')
    
    io_product_product = eurostat.load_supply_use_data(os.path.join(path, 'data', 'eurostat', 'economy', 'Symmetric input-output table at basic prices (product by product) (naio_10_cp1700)', 'naio_10_cp1700.tsv.gz'), 2015)
    io_product_product = io_product_product[io_product_product['stk_flow'] != 'TOTAL']
    
    io_branch_product = eurostat.load_supply_use_data(os.path.join(path, 'data', 'eurostat', 'economy', 'Supply table at basic prices incl. transformation into purchasers\' prices (naio_10_cp15)', 'naio_10_cp15.tsv.gz'), 2015)
    
    
    shops_surface = load_FS_commerce(os.path.join(path, 'data', 'insee', 'FS_ens_2016_2015','FS_Commerce_2016_2015.xls'),year)


    

