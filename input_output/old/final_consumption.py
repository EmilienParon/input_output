

def compute_gov_npish_coefs(final_consumption):

    # Extract the final consumption of the government and NPISH
    gov_npish_io = final_consumption[final_consumption['output_product'].isin(['P3_S15', 'P3_S13'])]

    # Make their final consumption an intermediate consumption of households
    gov_npish_prod = gov_npish_io.groupby('output_product', as_index=False)['value'].sum()
    gov_npish_prod['origin'] = 'DOM'
    gov_npish_prod['input_product'] = gov_npish_prod['output_product']
    gov_npish_prod['output_product'] = 'P3_S14'
    
    households_final_consumption = pd.concat([households_final_consumption, gov_npish_prod], sort=True)

    # Compute their IO coefficients and add them as activities in the technical coefficients dataframe
    gov_npish_io = pd.merge(gov_npish_io, gov_npish_prod[['input_product', 'value']], left_on='output_product', right_on='input_product')
    gov_npish_io['a_ij'] = gov_npish_io['value_x']/gov_npish_io['value_y']
    gov_npish_io['flow_type'] = 'consumption'
    gov_npish_tech_coef = gov_npish_io[['origin', 'flow_type', 'input_product_x', 'output_product', 'a_ij']]
    gov_npish_tech_coef.columns = ['origin', 'flow_type', 'input_product', 'output_product', 'a_ij']

    return gov_npish_tech_coef


def compute_households_emissions(emissions):
    
    hh_em = emissions[emissions['nace_r2'].isin(['HH_HEAT', 'HH_TRA', 'HH_OTH'])].copy()
    hh_em['value'] = hh_em['value']/66.42/1e6
    
    hh_em = hh_em.rename({"nace_r2":'output_product'},axis=1)
    #hh_em.columns = ['output_product', 'airpol', 'value']
    
    return hh_em


def compute_households_coefs(final_consumption):

	# Extract the final consumption of households
    households_final_consumption = final_consumption[final_consumption['output_product'] == 'P3_S14']


def compute_final_consumption_technical_coefs(io_product_product, product_list, branch_emissions):

	# ------------------------------
    # Final Consumption
    final_consumption = compute_final_consumption(io_product_product, product_list)

    
