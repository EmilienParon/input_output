#input_output is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import oemof.solph as oemof_solph
import uuid

class Market:

	def __init__(self, name):
		"""
			The Market object matches physical or monetary exchange nodes, where activities take their inputs from or inject their production.

			Args:
				name (str): the name of the market.
		"""
		self.name = name
		self.id = str(uuid.uuid1())
		self.oemof_bus = oemof_solph.Bus(label=self.id)
		self.system = None

	def setup(self, oemof_system):
		"""
            Add the oemof Bus component to the oemof model.
        """
		oemof_system.add(self.oemof_bus)

		self.system.nodes[self.id] = self.name
