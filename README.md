Input-Output
========================================
Input-Output is a package developed by Elioth (https://elioth.com/) to perform GHG emissions footprinting for urban projects, by combining background input-output activities modelled from monetary flows and specific local activities when process based LCA are available.

The tool is based on Oemof, which is originally used for energy systems modelling and extended here to simulate any kind of flow (energy, emissions, monetary, matter, functional unit...). Oemof makes it simple to setup and solve linear programming systems from descriptions of flows, and to balance all flows to perform the LCA analysis.

# Organization
The tool has the following parts :

## System
The main object of the package, unique for each LCA model. It stores territories, activities and the oemof simulation object.

## Territories
The system can have multiple production-consumption territories, where activities consume and produce service flows. Territories can be linked to model import and export of flows.

## Activities and services
Activities transform some input service flows into an output service flow. All kind of flows can be defined : CO2e flows emitted to the biosphere, energy supply flows, service flows... All kind of activities can thus be defined : pure consumer activities (household consumption), pure producer activities (energy production), or "transformer" activities (industry, shops, offices...).


## Data preparation functions
### Eurostat
Eurostat data provides monetary input-output data, as well as direct GHG emissions data, for 64 branches of the economy (equivalent to activities in this package). The package provides functions to format this data into a ready-to-use sqlite database of activities and their input - output service flows.

# Installation
## Install the CBC solver
The CBC solver is used by oemof to solve linear programming problems. You can install it with COIN, by downloading the binary for your platform (https://www.coin-or.org/download/binary/CoinAll/) and adding the path/to/coin/bin to your path.

## Create an Anaconda environment
Install Oemof.


License
---------------------

Copyright input_output by elioth 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.

You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

Contributors
---------------------
- [Félix Pouchain](https://gitlab.com/FlxPo)
- [Giuseppe Peronato](https://gitlab.com/gperonato)
- [Louise Gontier](https://gitlab.com/l.gontier)
